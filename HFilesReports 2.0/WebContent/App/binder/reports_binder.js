//containers
var root = "#root";
var appBody = "#reportContainer";

//basic widgets
var loadingScreen;
var successBox;
var errorBox;

//app widgets
var ssoWidget;
var reports;
var fileManager;
var generateReportManager;
var utilitiesMenu;
var utilTKReformatter;
var utilTNAParser;
var utilTNAReportGenerator;

//origin and web service urls
var pathArray = location.href.split('/');
var origin = pathArray[0] + '//' + pathArray[2]
var wsUrl = origin + "/" + (window.location.pathname.split("/")[1]) + "/svc/documents/report/";
var adminSvcBase = origin + '/hfiles-admin-web-svc/services/';
var hfilesSvcBase = origin + '/hfiles-web-svc/services/';
var urls = {
	getRole: function() {
		return adminSvcBase + "admin/role/" + roleId;
	},
	getCompanies: function() {
		return adminSvcBase + "admin/companies/" + roleId;
	},
	download: function(companyId, frequency, gfsKey, fileId) {
		return wsUrl + "download/" + companyId + "/" + frequency + "/" + gfsKey + "/" + fileId;
	},
	view: function(companyId, frequency, gfsKey, fileId) {
		return wsUrl + "view/" + companyId + "/" + frequency + "/" + gfsKey + "/" + fileId;
	},
	getFileList: function(companyId, frequency, reportType) {
		return wsUrl + "list/" + companyId + "/" + frequency + "/" + reportType;
	},
	runLoader: function(reportType) {
		return wsUrl + "runnable/" + reportType;
	}
};

//app variables
var roleId;
var bearerId = 'svi-token';
var bearer = null;

function buildAppBody() {
	var body = document.createElement("div");
	$(body).addClass("sso-appbody-container");
	$(body).attr("id", "reportContainer");
	$(root).append(body);
	
	buildSSO();
}

function buildSSO() {
	ssoWidget = $.extend(true, {}, new SSOplugin());
	$(root).append(ssoWidget.asWidget());

	var ssoInterval = setInterval(function() {
		roleId = ssoWidget.getRoleId();
		
		if (roleId) {
			clearInterval(ssoInterval);
			bearer = localStorage.getItem(bearerId);
			initializeBasicWidgets();
		}
	}, 1000);
}

function initializeBasicWidgets() {
	loadingScreen = $.extend(true, {}, new LoadingScreenWidget());
	$(root).append(loadingScreen.getWidget());
	
	successBox = $.extend(true, {}, new SuccessBox());
	$(root).append(successBox.getWidget());
	
	errorBox = $.extend(true, {}, new ErrorBox());
	$(root).append(errorBox.getWidget());
	
	retrieveCompanies();
}

function retrieveCompanies() {
	loadingScreen.showLoader("Retrieving companies...");
	sendRequest("GET", urls.getCompanies(), {}).statusCode({
        200: function(data) {
        	if (!$.isEmptyObject(data["companies"])) {
            	var companies = reformatCompanyList(data["companies"]);
            	lookupConfig["companies"] = companies;
            	initializeApp();
            	loadingScreen.hideLoader();
        	}
        },
        500: function(data) {
        	loadingScreen.hideLoader();
			showErrorBox("Failed", "Error in retrieving companies.");
        }
	});
}

function reformatCompanyList(data) {
	var companyList = {};
	
	for (var i = 0; i < data.length; i++) {
		var company = data[i];
		var companyId = company["company_id"];
		var companyName = company["company_name"];
		companyList[companyId] = companyName;
	}
	
	return companyList;
}

function initializeApp() {
	reports = $.extend(true, {}, new Reports());
	$(appBody).append(reports.getWidget());
	
	fileManager = $.extend(true, {}, new FileManager());
	fileManager.getExplorer().getCompany().customPopulate(lookupConfig.companies);
	$(reports.getContentWrapper()).append(fileManager.getWidget());
	
	generateReportManager = $.extend(true, {}, new GenerateReportManager());
	generateReportManager.getGenRepExplorer().getCompany().customPopulate(lookupConfig.companies);
	$(reports.getContentWrapper()).append(generateReportManager.getWidget());
	
	fileViewer = $.extend(true, {}, new ReportViewer());
	$(appBody).append(fileViewer.getWidget());
	
	utilitiesMenu = $.extend(true, {}, new UtilitiesMenu());
	$(reports.getContentWrapper()).append(utilitiesMenu.getWidget());
	
	tkReformaterModal = $.extend(true, {}, new OptionModal(utilTabConfig.tkReformatter));
	$(utilitiesMenu.getWidget()).append(tkReformaterModal.getWidget());
	
	tnaParserModal = $.extend(true, {}, new OptionModal(utilTabConfig.tnaParser));
	$(utilitiesMenu.getWidget()).append(tnaParserModal.getWidget());
	
	tnaReportGenModal = $.extend(true, {}, new OptionModal(utilTabConfig.tnaReportGenerator));
	$(utilitiesMenu.getWidget()).append(tnaReportGenModal.getWidget());
	
	setHandlers();
}

function setHandlers() {
	var reportsMenu = reports.getMenuWrapper();
	setReportsMenuHandler(reportsMenu);
	
	var mainFolders = fileManager.getExplorer().getMainFolderWidgets();
	setMainFolderHandlers(mainFolders);
	
	var subFolders = fileManager.getExplorer().getSubFolderWidgets();
	setSubFolderHandlers(subFolders);

	var options = utilitiesMenu.getUtilTabs().getOptions();
	setOptionsHandlers(options);
	
	var companyDropDown = fileManager.getExplorer().getCompany().getSelect();
	setCompanyDropdownHandler(companyDropDown);
}

function setCompanyDropdownHandler(dropdown){
	$(dropdown).on("change", function(){
		console.log("wazaaaaas");
		fileManager.getFileSummary().clearValues();
		
		
	});
}


function setReportsMenuHandler(reportsMenu) {
	$(reportsMenu).on("click tap", "button", function() {
		if ($(this).data("type") === "menu") {
			if (!$(this).hasClass("selected")) {
				var idx = $(this).data("idx");
				reports.selectMenu(idx);
				menuEvents(idx);
			}
		}
	});
	setDefaultTab();
}

function menuEvents(index) {
	switch(index) {
		case 0:
			showTNA();
	        break;
		case 1:
			showGenerateReport();
			break;
		case 2:
			showUtils();
			break;
		default:
			// add default menu
	}
}

function setDefaultTab() {
	reports.selectMenu(DEFAULT_MENU);
	menuEvents(DEFAULT_MENU);
}

function showTNA() {
	resetMenu();
	fileManager.show();
}

function showUtils() {
	resetMenu();
	utilitiesMenu.show();
}

function showGenerateReport() {
	resetMenu();
	generateReportManager.show();
}

function resetMenu() {
	utilitiesMenu.hide();
	fileManager.hide();
	generateReportManager.hide();
}

function setMainFolderHandlers(mainFolders) {
	for (var i = 0; i < mainFolders.length; i++) {
		var mainFolder = mainFolders[i];
		addMainFolderHandler(mainFolder);
	}
}

function addMainFolderHandler(mainFolder) {
	$(mainFolder.getFolder().getWidget()).on("click tap", function() {
		var subFolders = mainFolder.getSubFolders();
		
		if ($(subFolders).is(":visible")) {
			$(subFolders).hide();
		} else {
			$(subFolders).show();
		}
	});
}

function setSubFolderHandlers(subFolders) {
	for (var i = 0; i < subFolders.length; i++) {
		var subFolder = subFolders[i];
		addSubFolderHandler(subFolder);
	}
}

function addSubFolderHandler(subFolder) {
	$(subFolder.getWidget()).on("click tap", function() {
		var frequency = $(this).data("frequency");
		var reportType = $(this).data("reportType");
		var companyId = fileManager.getExplorer().getCompany().getValue();
		
		if (companyId == "") {
			showErrorBox("", "Select company first.");
		} else {
			retrieveFiles(companyId, frequency, reportType);
		}
	});
}

function retrieveFiles(companyId, frequency, reportType) {
	loadingScreen.showLoader("Retrieving files...");
	sendRequest("GET", urls.getFileList(companyId, frequency, reportType), {}).statusCode({
		200: function(data) {
			if (!$.isEmptyObject(JSON.parse(data))) {
				fileManager.getFileSummary().populate(reformatFileList(JSON.parse(data)));
				var fileWidgets = fileManager.getFileSummary().getFiles();
				setFileSummaryHandlers(fileWidgets, frequency);
			} else {
				fileManager.getFileSummary().clearValues();
				fileManager.getFileSummary().noFilesFound();
			}
			fileManager.getFileSummary().setHeader(reportType, frequency);
			loadingScreen.hideLoader();
		},
		500: function(data) {
			loadingScreen.hideLoader();
			showErrorBox("Failed", "An error has occurred. Please contact your system's administrator.");
		}
	});
}

function reformatFileList(data) {
	var files = [];
	for (var gfsKey in data) {
		var documents = data[gfsKey];
		for (fileId in documents) {
			var fileName = documents[fileId];
			var file = {};
			file["name"] = fileName;
			file["fileId"] = fileId;
			file["gfsKey"] = gfsKey;
			files.push(file);
		}
	}
	return files;
}

function setFileSummaryHandlers(files, frequency) {
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		addFileHandler(file, frequency);
	}
}

function addFileHandler(file, frequency) {
	var downloadBtn = file.getDownloadBtn();
	setDownloadHandler(downloadBtn, file, frequency);
	
	var viewBtn = file.getViewBtn();
	setViewHandler(viewBtn, file, frequency);
}

function setDownloadHandler(button, file, frequency) {
	$(button).on("click tap", function() {
		var fileId = file.getFileId();
		var gfsKey = file.getGfsKey();
		var companyId = fileManager.getExplorer().getCompany().getValue();
		downloadSelectedFile(companyId, frequency, gfsKey, fileId);
	});
}

function setViewHandler(button, file, frequency) {
	var fileType = file.getFileType().toLowerCase();

	if (isInArray(fileType, viewableFileTypes)) {
		$(button).on("click tap", function() {
			var fileName = file.getFileName();
			var fileId = file.getFileId();
			var gfsKey = file.getGfsKey();
			var companyId = fileManager.getExplorer().getCompany().getValue();
			var viewUrl = urls.view(companyId, frequency, gfsKey, fileId)
		
			showViewer(viewUrl, fileName);
		});
	} else {
		$(button).remove();
	}
}

function downloadSelectedFile(companyId, frequency, gfsKey, fileId) {
	var downloadUrl = urls.download(companyId, frequency, gfsKey, fileId)
	var win = window.open(downloadUrl);
}

function showViewer(url, reportType) {
	fileViewer.setViewerUrl(url);
	fileViewer.showViewer(reportType, "");
}

function setOptionsHandlers(options) {
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		addOptionHandler(option);
	}
}

function addOptionHandler(option) {
	$(option.getWidget()).on("click tap", function() {
		var optionId = option.getId();
		optionEvents(option, optionId);
	});
}

function optionEvents(option, optionId) {
	switch(optionId) {
		case "TK-REFORMATTER":
			setTKReformatterModal(option);
	        break;
		case "TNA-PARSER":
			setTNAParserModal(option);
			break;
		case "TNAREP":
			setTNAReportGenModal(option);
			break;
		default:
			//default option
	}
}

function setTKReformatterModal(option) {
	var id = option.getId();
	var title = option.getTitle();
	var description = option.getDescription();
	var parameters = option.getParameters();
	
	tkReformaterModal.setModalTitle(title);
	tkReformaterModal.setModalDescription(description);
	tkReformaterModal.setModalFields(parameters);

	setTKReformatterModalHandlers(id);
	
	tkReformaterModal.show();
}

function setTKReformatterModalHandlers(id) {
	var runBtn = tkReformaterModal.getRun().getButton();
	$(runBtn).on("click tap", function() {
		var fieldsData = tkReformaterModal.getModalData();
		runLoader(id, fieldsData);
	});
}

function setTNAParserModal(option) {
	var id = option.getId();
	var title = option.getTitle();
	var description = option.getDescription();
	var parameters = option.getParameters();
	
	tnaParserModal.setModalTitle(title);
	tnaParserModal.setModalDescription(description);
	tnaParserModal.setModalFields(parameters);
	
	var modalFields = tnaParserModal.getModalFields();
	populateModalFields(modalFields);
	
	setTNAParserModalHandlers(id);
	
	tnaParserModal.show();
}

function setTNAParserModalHandlers(id) {
	var runBtn = tnaParserModal.getRun().getButton();
	$(runBtn).on("click tap", function() {
		var fieldsData = tnaParserModal.getModalData();
		runLoader(id, fieldsData);
	});
}

function setTNAReportGenModal(option) {
	var id = option.getId();
	var title = option.getTitle();
	var description = option.getDescription();
	var parameters = option.getParameters();
	
	tnaReportGenModal.setModalTitle(title);
	tnaReportGenModal.setModalDescription(description);
	tnaReportGenModal.setModalFields(parameters);
	
	var modalFields = tnaReportGenModal.getModalFields();
	populateModalFields(modalFields);
	
	setTNAReportGenModalHandlers();
	
	tnaReportGenModal.show();
}

function setTNAReportGenModalHandlers(id) {
	var reportType = tnaReportGenModal.getModalFieldByKey("report_type");
	var reportFrequency = tnaReportGenModal.getModalFieldByKey("report_frequency");
	$(reportType.getSelect()).on("change", function() {
		id = reportType.getValue();
		
		if (isInArray(id, hasFrequency)) {
			reportFrequency.customPopulate(lookupConfig["reportFrequencies"][id]);
			reportFrequency.show();
		} else {
			reportFrequency.hide();
			reportFrequency.empty();
		}
	});
	
	var runBtn = tnaReportGenModal.getRun().getButton();
	$(runBtn).on("click tap", function() {
		var fieldsData = tnaReportGenModal.getModalData();
		runLoader(id, fieldsData);
	});
}

function populateModalFields(modalFields) {
	for (var key in modalFields) {
		var modalField = modalFields[key];
		var fieldType = modalField.getAttribute("fieldType");
		var lookup = modalField.getAttribute("lookup");

		if (fieldType === "list") {
			modalField.customPopulate(lookupConfig[lookup]);
		} else if (fieldType === "multipleSelect") {
			modalField.customPopulate(lookupConfig[lookup]);
			modalField.selectizeField();
		}
	}
}

function selectizeField(select) {
	var $select = $(select).selectize({
	    delimiter: ',',
	    persist: false,
		maxItems: null
	});
	
	return $select[0].selectize;
}

function runLoader(reportType, data) {
	var typeOfData = typeof data;
	
	if (typeOfData.toLowerCase() === "string") {
		showErrorBox("Failed", data);
	} else {
		loadingScreen.showLoader("Running loader...");
		sendRequest("POST", urls.runLoader(reportType), JSON.stringify(data)).statusCode({
			200: function(data) {
				loadingScreen.hideLoader();
				showSuccessBox("Success", "Successfully run the loader.");
			},
			401: function(data) {
				loadingScreen.hideLoader();
				showErrorBox("Failed", "Report type unavailable.");

			},
			500: function(data) {
				loadingScreen.hideLoader();
				showErrorBox("Failed", "An error has occurred. Please contact your system's administrator.");
			}
		});
	}
}

function isInArray(value, arrToCheck) {
	var flag = false;

	if ($.inArray(value, arrToCheck) !== -1) {
		flag = true;
	}

	return flag;
}

function showSuccessBox(title, msg) {
	successBox.setTitle(title);
	successBox.show(msg);
}

function showErrorBox(title, msg) {
	errorBox.setTitle(title);
	errorBox.show(msg);
}

function sendRequest(methodType, url, data, successCallBack, failCallBack) {	
	var promiseRequest;
	var ajaxParameters = {
			url: url,
			headers: {
				'Authorization': bearer
			},
			type: methodType,
			contentType: 'application/json; charset=UTF-8',
	};

	if (typeof data !== "undefined" || data.length > 0) {
		ajaxParameters.data = data;
	}

	promiseRequest = $.ajax(ajaxParameters);

	if (typeof successCallBack !== "undefined") {
		promiseRequest.done(successCallBack);
	}

	if (typeof failCallBack !== "undefined") {
		promiseRequest.fail(failCallBack);
	}
	return promiseRequest;
}

$(document).ready(function() {
	buildAppBody();
});