var menuConfig = {
	"tna": {
		"index": 0,
		"label": "Time and Attendance"
	},
	"genrep": {
		"index": 1,
		"label": "Generate Reports"
	}
//,
//	"utils": {
//		"index": 2,
//		"label": "Utilities"
//	}
};

var fileExplorerConfig = {
	"Man Hours": ["Weekly", "Monthly"],
	"Excess Hours": ["Daily"],
	"Attendance Sheet": ["Daily"],
	"Salary ForeCast": ["Weekly"]
};

var viewableFileTypes = ["tif", "tiff", "bmp", "jpg", "jpeg", "gif", "png", "eps", "pdf"];

var lookupConfig = {
	"reportTypes": {
		"TNAREP-MH": "Man Hours",
		"TNAREP-DTR": "Daily Time Record",
		"TNAREP-AS": "Attendance Sheet",
		"TNAREP-EH": "Excess Hours",
		"TNAREP-SF": "Salary Forecast"
	},
	"reportFrequencies": {
		"TNAREP-MH": {
			"W": "All Weeks",
			"M": "Whole Month",
			"MM": "Mid-Month",
			"ME": "Month-End"
		}, 
		"TNAREP-DTR": {
			"W1": "Week 1",
			"W2": "Week 2",
			"W3": "Week 3",
			"W4": "Week 4",
			"M": "WholeMonth",
		},
		"TNAREP-SF": {
			"W1": "Week 1",
			"W2": "Week 2",
			"W3": "Week 3",
			"W4": "Week 4"
		}
	}
	
}

var hasFrequency = ["TNAREP-MH", "TNAREP-DTR", "TNAREP-SF"];

var utilTabConfig = {
	"tkReformatter": {
		"id": "TK-REFORMATTER",
		"title": "TK File Reformatter",
		"description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		"parameters": [{
			"key": "date_covered_from",
			"label": "Date covered from",
			"type": "date",
			"required": true
		}, {
			"key": "date_covered_to",
			"label": "Date covered to",
			"type": "date",
			"required": true
		}]
	},
	"tnaParser": {
		"id": "TNA-PARSER",
		"title": "Time and Attendance Parser",
		"description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		"parameters": [{
			"key": "date_covered",
			"label": "Date covered",
			"type": "date",
			"required": true
		}, {
			"key": "company_ids",
			"label": "Company Ids",
			"type": "multipleSelect",
			"required": true,
			"lookup": "companies"
		}]
	}, 
	"tnaReportGenerator": {
		"id": "TNAREP",
		"title": "Time and Attendance Report Generator",
		"description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		"parameters": [{
			"key": "report_type",
			"label": "Report type",
			"type": "list",
			"required": true,
			"lookup": "reportTypes"
		}, {
			"key": "company_ids",
			"label": "Company Ids",
			"type": "multipleSelect",
			"required": true,
			"lookup": "companies"
		}, {
			"key": "date_covered",
			"label": "Date covered",
			"type": "date",
			"required": true
		}, {
			"key": "report_frequency",
			"label": "Report frequency",
			"type": "list",
			"required": true,
			"lookup": ""
		}]
	}
};

var calendarIconUrl = "App/assets/img/calendar.png";