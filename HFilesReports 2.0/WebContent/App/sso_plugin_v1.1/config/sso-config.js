/* Configuration File */

/* SWITCH */
IS_SSO_ACTIVE = 'N';
IS_HORIZONTAL = true;

/* SSO APPLICATION PATH */
//SSO_BASE_URL = 'https://52.202.44.59:8181/sso4-portal';
SSO_BASE_URL = 'https://falcon.svi.cloud:8181/sso';
SSO_LOGIN_URL = SSO_BASE_URL + '/login';
SSO_ACCOUNT_URL = SSO_BASE_URL + '/myAccount';
SSO_APPS_URL = SSO_BASE_URL + '/myApps'

/* SSO SERVLET PATH */
SERVLET_BASE_URL = 'http://52.202.44.59:8080/sso4-portal/services';
GET_USER_DATA = SERVLET_BASE_URL + '/info/details/current/full';
END_SESSION = SERVLET_BASE_URL + '/login/tokens/logout';
GET_ICON = SERVLET_BASE_URL + '/info/apps/icons/';

/* SSO CONSTANT */
TERMINATION_TIME_OUT = 30; /* Place value in seconds */
SSO_VERSION = '1.1'
APP_ASSET_PATH = 'App/sso_plugin_v' + SSO_VERSION + '/assets';
APP_LOGO_PATH = APP_ASSET_PATH + '/images/reports_icon.png';
APP_DEFAULT_USER_ICON_PATH = APP_ASSET_PATH + '/images/photo1.png';
APP_MODULE_NAME = "H-Files Reports";
APP_ROLE_ID_LIST = ['CLN', 'superadmin', 'hrhead', 'svi_hrhead', 'sscipdpm', 'iscipdpm', 'sscgh'];

/* DEV PATH */
DEV_BASE_URL = 'https://localhost:8181/SSODevSvc/svcs';
GET_DEV_COOKIE = DEV_BASE_URL + '/functions/cookies/dummy';
TEST_DEV_COOKIE = DEV_BASE_URL + '/functions/cookies/decoded';