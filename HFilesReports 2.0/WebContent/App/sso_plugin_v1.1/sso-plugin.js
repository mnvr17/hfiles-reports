var SSOplugin = function() {
    if (!(this instanceof SSOplugin)) {
        throw new Error("This needs to be called with the new keyword");
    }

    var global = {
        appObj: [],
        currentRoleId: '',
        currentUsername: '',
        userFirstName: '',
        baseUrl: document.location.protocol + '//' + document.location.host + document.location.pathname,
        bearerId : 'svi-token',
        idleSec: 0,
        isIdle: false,
        termSec: TERMINATION_TIME_OUT,
        imageUrl: APP_LOGO_PATH,
        moduleName: APP_MODULE_NAME,
        cssOrientKeyword: "",
        idleTimer: null,

        /* Main Elements */
        logoImage: null,

        profButton: null,
        userPhoto: null,
        userPhoto2: null,
        userLabel: null,
        userLabel2: null,
        roleLabel: null,
        roleLabel2: null,

        appsButton: null,

        customControlList: [],
        customWrap: null,
        customInner: null,
        customScroll: null,
        customButton: null,

        /* Popup Elements */
        profDlog: null,
        swtButton: null,
        accButton: null,
        logButton: null,

        appsDlog: null,
        appWrap: null,
        appScroll: null,

        customDlog: null,
        customDlogWrap: null,
        customDlogScroll: null,

        roleDlog: null,

        noRoleDlog: null,
        switchDlog: null,
        logoutDlog: null,
        idleDlog: null,

        notiyDlog: null,

        splash: null,

        wrapper: null,
        
        menuBtn: null,
		menuWrap: null
    };
    
    var menuCnfg = [{
		"id":"admin",
		"label":"Admin",
		"title":"Admin"
	}];

	var menuWdgtList = [];
	
    global.wrapper = generateWidget(); /* Instantiate Widget */

    function generateWidget() {
        if (!IS_HORIZONTAL) {
            global.cssOrientKeyword = "_ver";
        }

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-main-wrap sso-main-wrap' + global.cssOrientKeyword + ' sso-base-bg');

        var innerWrap = generateInitialWidgets();
        $(wrap).append(innerWrap);

        var profDl = generateProfPop();
        $(wrap).append(profDl);

        var appsDl = generateAppPop();
        $(wrap).append(appsDl);

        var customDlg = generateCustomPop();
        $(wrap).append(customDlg);

        var roleDl = global.roleDlog = $.extend(true, {}, new RoleSelector());

        var noRoleDl = global.noRoleDlog = $.extend(true, {}, new SsoNotification('Role Error'));

        var switchDl = global.switchDlog = $.extend(true, {}, new SsoQuestion('Switch Role'));

        var logoutDl = global.logoutDlog = $.extend(true, {}, new SsoQuestion('Confirm'));

        var notifyDl = global.notiyDlog = $.extend(true, {}, new SsoNotification('Notification'));

        var idleDl = global.idleDlog = $.extend(true, {}, new SsoNotification('Continue Session'));
        $(idleDl.getOkButton()).html('Continue My Session');
        $(idleDl.getOkButton()).addClass('sso-control-button_text sso-base-font');

        var splash = global.splash = $.extend(true, {}, new SsoLoading(global.imageUrl, 'Fetching data. Please wait...'));

        

        $(wrap).append(roleDl.asWidget());
        $(wrap).append(noRoleDl.asWidget());
        $(wrap).append(switchDl.asWidget());
        $(wrap).append(logoutDl.asWidget());
        $(wrap).append(notifyDl.asWidget());
        $(wrap).append(idleDl.asWidget());

        $(wrap).append(splash.asWidget());

        setHandler();
        return wrap;
    }
    
    function generateInitialWidgets() {
        var logo = global.logoImage = document.createElement('img');
        $(logo).addClass('sso-logo' + global.cssOrientKeyword);
        $(logo).attr('src', global.imageUrl);
        
        var logoTxt = document.createElement("button");
        $(logoTxt).addClass("sso-logo-text");
        $(logoTxt).text(global.moduleName);

        var ssoControl = generateSsoControls();
        var scrollWrap = generateCustomControl();

        var wrap = document.createElement('div');
        // $(wrap).addClass('sso-inner-wrap sso-inner-wrap' +
		// global.cssOrientKeyword);
        $(wrap).addClass('innerContainer');
        
        $(wrap).append(logo);
        $(wrap).append(logoTxt);
        
//        $(wrap).append(constructNavMenu());
//		
//		$(wrap).append(constructHamburgerMenu());
		
        $(wrap).append(ssoControl);
        $(wrap).append(scrollWrap);    
        return wrap;
    }
    
    function constructNavMenu(){
		var innerPnl = global.menuWrap = document.createElement("div");
		$(innerPnl).addClass("sso-menu-wrap");
		
		for(var m in menuCnfg){
			var menu = menuCnfg[m];
			var id = menu.id;
			var label = menu.label;
			var title = menu.title;
			
			var menuBtn = document.createElement("div");
			var menuIcon = document.createElement("div");
			var menuTxt = document.createElement("div");
			
			$(menuBtn).addClass("sso-menu-btn-dflt");
			$(menuIcon).addClass("sso-menu-btn-icon");
			$(menuTxt).addClass("sso-menu-btn-lbl");
			
			$(menuBtn).attr("title",title);
			$(menuIcon).attr("id",id);
			$(menuTxt).text(label);
			
			$(menuBtn).append(menuIcon);
			$(menuBtn).append(menuTxt);
			//$(menuBtn).on("click tap",selectMenuBtn);
			$(innerPnl).append(menuBtn);
			
			menuWdgtList.push(menuBtn);
		}
		return innerPnl;
	}
	
    function constructHamburgerMenu(){
		var menuBtn = global.menuBtn =document.createElement("button");
		$(menuBtn).addClass("sso-hambrgr-menu");
		$(menuBtn).on("click tap",function(){
			if($(global.menuWrap).is(":visible")){
				$(global.menuWrap).fadeOut();
			}else{
				$(global.menuWrap).fadeIn();
			}
		});
		
		return global.menuBtn;
	}
    
	function selectMenuBtn(btn){
		$(".sso-menu-btn-icon").each(function(i, obj) {
			var iconUrl = $(obj).css("background-image");
			var newUrl = iconUrl.replace('white', 'light');
			$(obj).css("background-image",newUrl);
			$(obj).next(".sso-menu-btn-lbl").removeClass("sso-menu-btn-lbl-active");
		});
		$(".sso-menu-btn-dflt").removeClass("sso-menu-btn-active");
		
		var selctdIcon = $(btn).find(".sso-menu-btn-icon");
		var selctdLbl = $(btn).find(".sso-menu-btn-lbl");
		
		var iconUrl = $(selctdIcon).css("background-image");
		var newUrl = iconUrl.replace('light', 'white');
		$(selctdIcon).css("background-image",newUrl);
		$(selctdLbl).addClass("sso-menu-btn-lbl-active");
		$(btn).addClass("sso-menu-btn-active");
	}
	
    function generateSsoControls() {
        var profBtn = global.profButton = generateProfile();
        var appBtn = generateApps();

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-ctrl sso-ctrl' + global.cssOrientKeyword);
        $(wrap).append(profBtn);
        $(wrap).append(appBtn);    
        return wrap;
    }

    function generateProfile() {
        var photo = global.userPhoto = document.createElement('img');
        $(photo).attr('src', APP_DEFAULT_USER_ICON_PATH);

        var photoWrap = document.createElement('div');
        $(photoWrap).addClass('sso-prof-imgWrap sso-prof-imgWrap' + global.cssOrientKeyword);
        $(photoWrap).append(photo);

        var name = global.userLabel = document.createElement('button');
        $(name).addClass('sso-base-font sso-base-font-color');
        $(name).addClass('prof-user');
        $(name).html('- - -');

        var role = global.roleLabel = document.createElement('button');
        $(role).addClass('sso-base-font sso-base-subfont-color');
        $(role).addClass('prof-role');
        $(role).html('- - -');

        var labelWrap = document.createElement('div');
        $(labelWrap).addClass('sso-btn-lblWrap sso-btn-lblWrap' + global.cssOrientKeyword);
        $(labelWrap).append(name);
        $(labelWrap).append(role);

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-btn-prof sso-btn-prof' + global.cssOrientKeyword);
        $(wrap).append(photoWrap);
        $(wrap).append(labelWrap);
        return wrap;
    }

    function generateApps() {
        var icon = global.appsButton = document.createElement('div');
        $(icon).addClass('myapp-icon');

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-app-btn sso-app-btn' + global.cssOrientKeyword);
        $(wrap).append(icon);
        return wrap;
    }

    function generateCustomControl() {
        var customWrap = global.customWrap = document.createElement('div');
        $(customWrap).addClass('sso-custom-ctrl sso-custom-ctrl' + global.cssOrientKeyword);

        var customInner = global.customInner = document.createElement('div');
        $(customInner).addClass('sso-custom-scrl sso-custom-scrl' + global.cssOrientKeyword);
        $(customInner).append(customWrap);

        var customScroll = global.customScroll = new IScroll(customInner, {
            click: true,
            scrollX: function() {
                if (IS_HORIZONTAL) {
                    return false;
                } else {
                    return true;
                }
                
            },
            scrollY: function() {
                if (IS_HORIZONTAL) {
                    return true;
                } else {
                    return false;
                }
                
            },
            scrollbars: true,
            interactiveScrollbars: true,
            mouseWheel: true,
            preventDefault: false,
        });

        if (IS_HORIZONTAL) {
            customScroll.options.scrollX = true;
            customScroll.options.scrollY = false;
        }

        var customButton = global.customButton = document.createElement('button');
        $(customButton).addClass('sso-menu-btn sso-menu-btn' + global.cssOrientKeyword);

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-custom-ctrl-wrap sso-custom-ctrl-wrap' + global.cssOrientKeyword);
        $(wrap).append(customButton);
        $(wrap).append(customInner);
        return wrap;
    }

    function generateProfPop() {
        var arw = document.createElement('div');
        $(arw).addClass('sso-dlog-arw sso-dlog-arw sso-dlog-arw sso-dlog-arw' + global.cssOrientKeyword + ' arw-pointer'  + global.cssOrientKeyword);

        var profile = generateMobileProfile();

        var roleBtn = global.swtButton = document.createElement('button');
        $(roleBtn).addClass('sso-prof-btn sso-btn-switch sso-base-font');
        $(roleBtn).html('Switch Role');

        var accBtn = global.accButton = document.createElement('button');
        $(accBtn).addClass('sso-prof-btn sso-btn-account sso-base-font');
        $(accBtn).html('My Account');

        var logBtn = global.logButton = document.createElement('button');
        $(logBtn).addClass('sso-prof-btn sso-btn-logout sso-base-font');
        $(logBtn).html('Sign Out');

        var scroll = document.createElement('div');
        $(scroll).addClass('sso-dlog-scrl sso-dlog-scrl' + global.cssOrientKeyword);
        $(scroll).append(profile);
        $(scroll).append(roleBtn);
        $(scroll).append(accBtn);
        $(scroll).append(logBtn);

        var wrap = global.profDlog = document.createElement('div');
        $(wrap).addClass('sso-dlog sso-dlog-prof dlog-hide');
        $(wrap).append(arw);
        $(wrap).append(scroll);
        return wrap;
    }

    function generateMobileProfile() {
        var photo = global.userPhoto = document.createElement('img');
        $(photo).attr('src', APP_DEFAULT_USER_ICON_PATH);

        var photoWrap = document.createElement('div');
        $(photoWrap).addClass('sso-prof-imgWrap');
        $(photoWrap).append(photo);

        var name = global.userLabel2 = document.createElement('button');
        $(name).addClass('sso-base-font sso-base-font-color sso-btn-lblWrap-mob');
        $(name).addClass('prof-user');
        $(name).html('- - -');

        var role = global.roleLabel2 = document.createElement('button');
        $(role).addClass('sso-base-font sso-base-subfont-color sso-btn-lblWrap-mob');
        $(role).addClass('prof-role');
        $(role).html('- - -');

        var labelWrap = document.createElement('div');
        $(labelWrap).addClass('sso-btn-lblWrap');
        $(labelWrap).append(name);
        $(labelWrap).append(role);

        var line = document.createElement('div');
        $(line).addClass('sso-prof-line');

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-prof-hd-wrap sso-prof-hd-wrap' + global.cssOrientKeyword);
        $(wrap).append(photoWrap);
        $(wrap).append(labelWrap);
         $(wrap).append(line);
        return wrap;
    }

    function generateAppPop() {
        var arw = document.createElement('div');
        $(arw).addClass('sso-dlog-arw sso-dlog-arw' + global.cssOrientKeyword + ' arw-pointer'  + global.cssOrientKeyword);

        var apps = global.appWrap = document.createElement('div');
        $(apps).addClass('sso-dlog-wrap');

        var scroll = document.createElement('div');
        $(scroll).attr('id', 'appWraper');
        $(scroll).addClass('sso-dlog-scrl sso-dlog-scrl'  + global.cssOrientKeyword);
        $(scroll).append(apps);

        var appScroll = global.appScroll = new IScroll(scroll, {
            click: true,
            scrollbars: true,
            interactiveScrollbars: true,
            mouseWheel: true,
            preventDefault: false,
        });

        var wrap = global.appsDlog = document.createElement('div');
        $(wrap).addClass('sso-dlog sso-dlog-app dlog-hide');
        $(wrap).append(arw);
        $(wrap).append(scroll);
        return wrap;
    }

    function generateCustomPop() {
        var arw = document.createElement('div');
        $(arw).addClass('sso-dlog-arw sso-dlog-arw' + global.cssOrientKeyword + ' arw-pointer'  + global.cssOrientKeyword);

        var container = global.customDlogWrap = document.createElement('div');
        $(container).addClass('sso-dlog-wrap');

        var scroll = document.createElement('div');
        $(scroll).attr('id', 'controlWraper');
        $(scroll).addClass('sso-dlog-scrl sso-dlog-scrl'  + global.cssOrientKeyword);
        $(scroll).append(container);

        var customScroll = global.customDlogScroll = new IScroll(scroll, {
            click: true,
            scrollbars: true,
            interactiveScrollbars: true,
            mouseWheel: true,
            preventDefault: false,
        });

        var wrap = global.customDlog = document.createElement('div');
        $(wrap).addClass('sso-dlog sso-dlog-cstm dlog-hide');
        $(wrap).append(arw);
        $(wrap).append(scroll);
        return wrap;
    }        

    function setHandler() {
        window.addEventListener('storage', processEvent, false);    
        $(global.profButton).click('click tap', function(event) {
            var profDlog = global.profDlog;
            showDialog(event.target, profDlog);
            event.stopPropagation();
        });

        $(global.appsButton).click('click tap', function(event) {
            var appsDlog = global.appsDlog;
            var parent = $(event.target).parent();

            showDialog(parent, appsDlog);
            event.stopPropagation();
            global.appScroll.refresh();
        });

        $(global.customButton).click('click tap', function(event) {
            var controlDlog = global.customDlog;
            var parent = $(event.target).parent();

            showDialog(event.target, controlDlog);
            event.stopPropagation();
            global.customDlogScroll.refresh();
        });

        $(global.swtButton).click('click tap', function() {
            var switchDlog = global.switchDlog;
            var msg = 'Switching to another role requires this app to be reloaded. Proceed?'

            hideDialogs();
            switchDlog.setMessage(msg);
            switchDlog.show();
        });

        $(global.accButton).click('click tap', runManagementAcc);

        $(global.logButton).click('click tap', function(event) {
            var widget = global.logoutDlog;
            var msg = 'Are you sure you want to logout and end your session?';

            hideDialogs();
            widget.setMessage(msg);
            widget.show();
        });

        $(document).click(function(e) {
            var profDlog = global.profDlog;
            var appsDlog = global.appsDlog;

            if (!$(e.target).is(profDlog)) {
                hideDialogs();
            } else if (!$(e.target).is(appsDlog)) {
                hideDialogs();
            }

        });

        $(window).on('resize', function() {
            var winWid = $(window).width();

            hideDialogs();

            if (winWid <= 1000) {
              
            } else {
               
            }

        })

        setModalHandler();
    }

    function setModalHandler() {
        var roleDlog = global.roleDlog;
        $(roleDlog.getOkButton()).click('click tap', function(event) {
            var obj = roleDlog.getSelectedRole();
            var roleLabel = global.roleLabel;
            var roleLabel2 = global.roleLabel2;

            if (obj) {
                var roleName = obj.roleName;
                global.currentRoleId = obj.roleId;

                roleDlog.hide();
                $(roleLabel).html(roleName);
                $(roleLabel2).html(roleName);
            }
        });

        var switchDlog = global.switchDlog;
        $(switchDlog.getOkButton()).click('click tap', function(event) {
            location.reload();
        });

        $(switchDlog.getCancelButton()).click('click tap', function(event) {
            switchDlog.hide();
        });

        var logoutDlog = global.logoutDlog;
        $(logoutDlog.getOkButton()).click('click tap', function(event) {
            endSessionServlet();
        });

        $(logoutDlog.getCancelButton()).click('click tap', function(event) {
            logoutDlog.hide();
        });

        var notiyDlog = global.notiyDlog;
        $(notiyDlog.getOkButton()).click('click tap', function(event) {
            notiyDlog.hide();
        });

        var idleDlog = global.idleDlog;
        $(idleDlog.getOkButton()).click('click tap', function(event) {
        	resetIdleTimer();         
        	global.isIdle = false;
            global.termSec = TERMINATION_TIME_OUT;
            global.idleDlog.hide();    	
            setTimerHandlers();
        });

        setTimerHandlers();

        getUserData();
    }
    
    function setTimerHandlers(){
    	$(document).click('click', function() {
        	resetIdleTimer()
        });
        $(document).mousemove(function() {
        	resetIdleTimer();
        });
    }
    
    function unsetTimerHandlers(){
        $(document).unbind('mousemove');
        $(document).unbind('click');
    }

    function showDialog(clickElem, dialogElem) {
        var isDisplay = $(dialogElem).is(":visible");

        if (!isDisplay) {
            hideDialogs();
            $(dialogElem).removeClass('dlog-hide');

            var postObj;

            if (IS_HORIZONTAL) {
                postObj = toCenterPosition(clickElem, dialogElem);
            } else {
                postObj = toDropRight(clickElem, dialogElem);
            }

            $(dialogElem).offset(postObj);

            $(dialogElem).addClass('animated fadeIn');
        } else {
            hideDialogs();
        }
    }

    function hideDialogs() {
        $(global.profDlog).addClass('dlog-hide');
        $(global.appsDlog).addClass('dlog-hide');
        $(global.customDlog).addClass('dlog-hide');
        $(global.profDlog).removeClass('animated bounceIn');
        $(global.appsDlog).removeClass('animated bounceIn');
        $(global.customDlog).removeClass('animated bounceIn');
    }

    function toCenterPosition(clickElem, dialogElem) {

        var elemPos = $(clickElem).offset();
        var elemWd = $(clickElem).width();
        var elemHt = $(clickElem).height();
        var diaWd = $(dialogElem).width();
        var arwElem = $(dialogElem).children().first(); // Get arrow element
														// from pop up //

//        console.log(clickElem);
//        console.log(elemPos);

        var winWd = $(window).width();

        var adjustment = (elemWd / 2) - (diaWd / 2);

        var newLeftPos = elemPos.left + adjustment;
        var newTopPos = elemPos.top + elemHt;

        var calcLeft = newLeftPos + diaWd;

        if (calcLeft > winWd) {
            newLeftPos = (elemPos.left + elemWd) - diaWd;
            $(arwElem).addClass('arw-reverse-loc' + global.cssOrientKeyword);
        } else {
            $(arwElem).removeClass('arw-reverse-loc' + global.cssOrientKeyword);
        }

        var postObj = {
            left: newLeftPos,
            top: newTopPos
        };

        return postObj;
    }

    function toDropRight(clickElem, dialogElem) {
        var elemPos = $(clickElem).offset();
        var elemWd = $(clickElem).width();
        var elemHt = $(clickElem).height();
        var diaHt = $(dialogElem).height();
        var arwElem = $(dialogElem).children().first(); // Get arrow element
														// from pop up //

        var winHt = $(window).height();

        // var adjustment = (elemWd / 2) - (diaWd / 2);

        var newLeftPos = (elemPos.left + elemWd);
        var newTopPos = elemPos.top;

        var calcTop = newTopPos + diaHt;

        if (calcTop > winHt) {
            newTopPos = (elemPos.top + elemHt) - diaHt;
            $(arwElem).addClass('arw-reverse-loc' + global.cssOrientKeyword);
        } else {
            $(arwElem).removeClass('arw-reverse-loc' + global.cssOrientKeyword);
        }

        var postObj = {
            left: newLeftPos,
            top: newTopPos
        };
       
        return postObj;
    }

    function runManagementAcc() {
        var url = SSO_ACCOUNT_URL;
        var notiyDlog = global.notiyDlog;
        var msg = 'Feature currently unavailable. Sorry for the inconvenience.'

        hideDialogs();
        notiyDlog.setMessage(msg);
        notiyDlog.show();
    }

    function changeUrl(path) {
        var url = path;
        var win = window.open(url, "_self");
        win.focus();
    }
    
    function getDevCookie(){
   	 $.ajax({
            url: GET_DEV_COOKIE,
            type: 'GET',
            success: function(result) {
           	 $.ajax({
                    url: TEST_DEV_COOKIE,
                    type: 'GET',
                    success: function(result) {
//                        console.log(result.responseText);
                    },
                    error: function(){
                    	console.log('error in testing dev cookie');
                    }
           	 });
            },
            error: function(){
            	console.log('error in retrieving dev cookie');
            }
   	 });
   }

    function getUserData() {
    	var bearer = localStorage.getItem(global.bearerId);
        var userData;
        $.ajax({
            url: GET_USER_DATA,
            headers: {'Authorization':bearer},
            success: function(result) {
                userData = global.appObj = result;
                startIdleTimer(userData.timeout);
                setUser(userData);
            },
            error: function(result) {
                var notiyDlog = global.notiyDlog;
                var msg = 'Error loading account details. Loading demo data.'

                hideDialogs();
                notiyDlog.setMessage(msg);
                notiyDlog.show();
                
            	if(IS_SSO_ACTIVE === 'Y'){
                    var url = SSO_LOGIN_URL + '?app=' + global.baseUrl;
                    changeUrl(url);            		
            	}else{
            		getDevCookie();
                    userData = global.appObj = {
                        'username': 'rpantan',
                        'fname': 'Rodgene',
                        'roles': [{
                                'roleId': 'CLN',
                                'roleName': 'Client'
                            },
                            {
                                'roleId': 'CSH',
                                'roleName': 'Cashier'
                            }
                        ],
                        'apps': [{
                                "id": "BPI_01",
                                "name": "Workflow",
                                "icon": "http://www.freeiconspng.com/uploads/courses-icon-10.png",
                                "url": "file:///C:/Users/rpantan/Desktop/Project%20Files/Coversheets%20Files/SSO%20Widget/test_index.html"
                            },
                            {
                                "id": "BPI_02",
                                "name": "Admin Tool",
                                "icon": "http://www.freeiconspng.com/uploads/clock-icon--small--flat-iconset--paomedia-5.png",
                                "url": "file:///C:/Users/rpantan/Desktop/Project%20Files/App%20-%20SSO/SSO%20Admin%20Tool/version%201.0/sso_admin.html"
                            }
                        ]
                    };
                    setUser(userData);
                    startIdleTimer(userData.timeout);            		
            	}
            }
        });
    }

    function setUser(userData) {
        var username = global.currentUsername = userData.username;
        var fname = global.userFirstName = userData.fname;
        var roles = userData.roles;
        var apps = userData.apps;

        if (username) {
            var delay = 500;
            var name = global.userLabel;
            var name2 = global.userLabel2;

            $(name).html(fname);
            $(name2).html(fname);

            setTimeout(function() {
                global.splash.hide();
            }, delay);

            delay = delay + 100;

            setTimeout(function() {
                setRole(roles);
                setApps(apps);
            }, delay);
        } else {
            var url = SSO_LOGIN_URL + '?app=' + global.baseUrl;
            changeUrl(url);
        }
    }

    function setRole(roles) {
        var appRoles = APP_ROLE_ID_LIST;
        var viewRoles = [];
        var roleLabel = global.roleLabel;
        var roleLabel2 = global.roleLabel2;

        for (var i = 0; i < roles.length; i++) {
            var role = roles[i];
            var roleId = role.roleId;

            for (var j = 0; j < appRoles.length; j++) {
                var comparedRole = appRoles[j];

                if (comparedRole == roleId) {
                    viewRoles.push(role);
                }
            };
        };

        if (viewRoles.length > 0) {
            var roleDlog = global.roleDlog;
            var switchButton = global.swtButton;

            if (viewRoles.length > 1) {
                roleDlog.show()
                roleDlog.setRoles(viewRoles);
            } else {
                var obj = viewRoles[0];
                var name = obj.roleName;
                global.currentRoleId = obj.roleId;

                $(roleLabel).html(name);
                $(roleLabel2).html(name);

                $(switchButton).hide();
            }

        } else {
            var noRoleDlog = global.noRoleDlog;
            var msg = 'Access restricted. Unable to identify matching role(s) from your account.';

            $(noRoleDlog.getOkButton).hide();
            noRoleDlog.setMessage(msg);
            noRoleDlog.show();
        }
    }

    function setApps(appList) {
        var appCount = appList.length;

        $(global.appWrap).empty();

        for (var i = 0; i < appList.length; i++) {
            var obj = appList[i];
            var id = obj.id;
            var name = obj.name;
            var icon = obj.icon;
            var url = obj.url;

            var widget = buildIcon(id, name, icon, url);
            $(global.appWrap).append(widget);
        };

        $(global.appsCount).html(appCount);
        global.appScroll.refresh();
    }

    function buildIcon(appId, appName, appIcon, url) {
        var icon = document.createElement('img');
        $(icon).attr('src', "data:image/png;base64," + appIcon);

        var name = document.createElement('button');
        $(name).addClass('sso-base-font');
        $(name).html(appName);

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-app-wrap');
        $(wrap).data('appId', appId);
        $(wrap).data('url', url);
        $(wrap).append(icon);
        $(wrap).append(name);
        $(wrap).on('click tap', runSelectedApp);
        return wrap;
    }

    function runSelectedApp(event) {
        var elem = event.target;
        var url = $(elem).data('url');
        var win = window.open(url, '_blank');

        if (win) {
            win.focus();
        } else {
            msg = 'Please allow popups for this website';
            global.ssoAlert.setMessage(msg);
            global.ssoAlert.show();
        }
    }

    function endSessionServlet() {
    	var bearer = localStorage.getItem(global.bearerId);
        $.ajax({
            url: END_SESSION,
            headers: {'Authorization':bearer},
            type: 'POST',
            complete: function() {
                localStorage.setItem('logged_in', 'false');
                localStorage.removeItem(bearerId);
            	var url = SSO_LOGIN_URL + '?app=' + global.baseUrl;
                changeUrl(url);
            }
        });
    }
    
    function resetIdleTimer(){
    	localStorage.setItem('timer', Math.random());
    	global.idleSec = 0;
    }

    function processEvent(event) {
        if (event.key === 'logged_in') {
            if (event.newValue != 'true') {
                var url = SSO_LOGIN_URL + '?app=' + global.baseUrl;
                changeUrl(url);
            }
        }else if(event.key === 'timer'){
        	global.idleSec = 0;
        	if(global.isIdle === true){
            	global.isIdle = false;
                global.termSec = TERMINATION_TIME_OUT;
                global.idleDlog.hide();   
        	}  	
        }
    }

    function startIdleTimer(timeout) {
    	localStorage.setItem('timer', Math.random());
        var timer = global.idleTimer;

        timer = setInterval(function() {
            var dlog = global.idleDlog;
            var msg = 'You have been inactive for a few minutes now. Terminating your session in <span class="sso-emphasize">';
            var sec = global.idleSec;
            var isIdle = global.isIdle;
            var termTime = global.termSec;

            if (sec >= timeout) {
                isIdle = true;
            } else {
                sec = sec + 1;
                global.idleSec = sec;
//                console.log(global.idleSec);
            }

            if (isIdle) {
                termTime = termTime - 1;
                global.termSec = termTime;

                var newMsg = msg + termTime + '</span> second(s)';

                dlog.setMessage(newMsg);
                dlog.show();
                unsetTimerHandlers();

                if (termTime == 0) {
                    clearInterval(timer);
                    endSessionServlet();
                }
            }

            global.isIdle = isIdle;

        }, 1000);
    }

    SSOplugin.prototype.getRoleId = function() {
        return global.currentRoleId;
    }
    
    SSOplugin.prototype.getFirstName = function() {
        return global.userFirstName;
    }
    
    SSOplugin.prototype.getUserName = function() {
        return global.currentUsername;
    }    
    
    SSOplugin.prototype.selectMenuBtn = function(menu) {
        selectMenuBtn(menu);
    }
    
    SSOplugin.prototype.addControl = function(elem) {
        global.customControlList.push(elem);

        var clone = $(elem).clone(true, true);
        $(clone).addClass('headerButton_mob');

        $(global.customWrap).append(elem);
        $(global.customDlogWrap).append(clone);

        global.customScroll.refresh();
        global.customDlogScroll.refresh();
    }

    SSOplugin.prototype.showSSOAlert = function(msg) {
        global.notiyDlog.setMessage(msg);
        global.notiyDlog.show();    
    }

    SSOplugin.prototype.hideSSOAlert = function() {
        global.notiyDlog.setMessage('');
        global.notiyDlog.hide();    
    } 
    
    SSOplugin.prototype.getRoleId = function() {
        return global.currentRoleId;
    }
	
    SSOplugin.prototype.showLoading = function() {
        global.splash.show();
    }   

    SSOplugin.prototype.hideLoading = function(elem) {
        global.splash.hide();
    }   

    SSOplugin.prototype.asWidget = function() {
        return global.wrapper;
    }

}

/*
 * SSO Widgets Placed under the main widget to avoid adding additional script to
 * the application currently using the plug-in
 */

var RoleSelector = function() {
    if (!(this instanceof RoleSelector)) {
        throw new Error("This needs to be called with the new keyword");
    }

    var global = {
        selectElemList: [],

        roleWrap: null,
        closeButton: null,
        okButton: null,
        wrapper: null,
        glassWrapper: null,
    };

    global.glassWrapper = generateWidget(); /* Instantiate Widget */

    function generateWidget() {
        var winWd = $(window).width();
        var winHt = $(window).height();

        var dlog = generateDialogbox();

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-noti-wrap noti-hide');
        $(wrap).width(winWd);
        $(wrap).height(winHt);

        $(wrap).append(dlog);
        return wrap;
    }

    function generateDialogbox() {
        var title = document.createElement('button');
        $(title).addClass('sso-header-label sso-base-font');
        $(title).html('Login As');

        var close = global.closeButton = document.createElement('button');
        $(close).addClass('sso-header-button sso-base-font');
        $(close).hide();

        var header = document.createElement('div');
        $(header).addClass('sso-selector-header sso-base-font');
        $(header).append(title);
        $(header).append(close);

        var container = global.roleWrap = document.createElement('div');
        $(container).addClass('sso-selector-body');

        var submit = global.okButton = document.createElement("button");
        $(submit).addClass('sso-control-button activeButton sso-base-font');

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-selector-wrap animated bounceIn');
        $(wrap).append(header);
        $(wrap).append(container);
        $(wrap).append(submit);
        return wrap;
    }

    RoleSelector.prototype.setRoles = function(roleObjList) {
        var wrap = global.roleWrap;
        var selectElems = global.selectElemList;
        var defaultSet = false;

        $(wrap).empty();

        for (var i = 0; i < roleObjList.length; i++) {
            var obj = roleObjList[i];
            var roleName = obj.roleName;
            var roleId = obj.roleId;

            var icon = document.createElement("div");

            var user = document.createElement("button");
            $(user).addClass('sso-base-font');
            $(user).html(roleName);

            var sub = document.createElement("button");
            $(sub).addClass('sso-base-font');
            $(sub).html(roleId);

            var select = document.createElement("button");
            $(select).addClass('user-select activeButton');
            $(select).data('object', obj);
            $(select).on('click tap', changeSelect);
            selectElems.push(select);

            if (!defaultSet) {
                $(select).trigger('click');
                defaultSet = !defaultSet;
            }

            var checkWrap = document.createElement("div");
            $(checkWrap).addClass('sso-select-wrap');
            $(checkWrap).append(icon);
            $(checkWrap).append(user);
            $(checkWrap).append(sub);
            $(checkWrap).append(select);

            $(wrap).append(checkWrap);
        };

    }

    function changeSelect(event) {
        var elemList = global.selectElemList;
        var className = 'sso-selector-selected'
        var elem = event.target;

        for (var i = 0; i < elemList.length; i++) {
            var select = elemList[i];
            $(select).removeClass(className);
            $(select).data('selected', false);
        };

        $(elem).addClass(className);
        $(elem).data('selected', true);
    }

    RoleSelector.prototype.show = function() {
        var glass = global.glassWrapper;
        var postObj = {
            left: 0,
            top: 0
        };

        $(glass).removeClass('noti-hide');
        $(glass).addClass('animated fadeIn');
        $(glass).offset(postObj);
    }

    RoleSelector.prototype.hide = function() {
        var glass = global.glassWrapper;
        $(glass).addClass('noti-hide');
    }

    RoleSelector.prototype.getSelectedRole = function() {
        var elemList = global.selectElemList;
        var selectedObj;

        for (var i = 0; i < elemList.length; i++) {
            var select = elemList[i];
            var isSelected = $(select).data('selected');

            if (isSelected) {
                selectedObj = $(select).data('object');
            }

        };
        return selectedObj;
    }

    RoleSelector.prototype.getCloseButton = function() {
        return global.closeButton;
    }

    RoleSelector.prototype.getOkButton = function() {
        return global.okButton;
    }

    RoleSelector.prototype.asWidget = function() {
        return global.glassWrapper;
    }
}

var RoleCheckbox = function(optionList, defaultValue) {
    if (!(this instanceof RoleCheckbox)) {
        throw new Error("This needs to be called with the new keyword");
    }

    var global = {
        options: optionList,
        selected: defaultValue,
        rolesList: [],

        wrapper: null
    };

    global.wrapper = generateWidget(); /* Instantiate Widget */

    function generateWidget() {
        var title = document.createElement('button');
        $(title).addClass('sso-role-title sso-base-font');
        $(title).html('Login As');

        var container = generateCheckbox();

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-role-wrap');

        $(wrap).append(title);
        $(wrap).append(container);
        return wrap;
    }

    function generateCheckbox() {
        var options = global.options
        var defaultVal = global.selected;
        var list = global.rolesList;

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-role-container');

        for (var i = 0; i < options.length; i++) {
            var value = options[i];

            var option = document.createElement('button');
            $(option).addClass('sso-role-btn sso-base-font');
            $(option).html(value);

            if (defaultVal == value) {
                $(option).addClass('role-active');
            } else {
                list.push(option);
            }

            $(wrap).append(option);
        };

        return wrap;
    }

    RoleCheckbox.prototype.getRoleButtons = function() {
        return global.rolesList;
    }

    RoleCheckbox.prototype.asWidget = function() {
        return global.wrapper;
    }
}

var SsoNotification = function(dialogTitle, message) {
    if (!(this instanceof SsoNotification)) {
        throw new Error("This needs to be called with the new keyword");
    }

    var global = {
        title: dialogTitle,
        msg: message,

        okButton: null,
        messageLabel: null,
        wrapper: null,
        glassWrapper: null,
    };

    global.glassWrapper = generateWidget(); /* Instantiate Widget */

    function generateWidget() {
        var winWd = $(window).width();
        var winHt = $(window).height();

        var dlog = generateDialogbox();

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-noti-wrap noti-hide');
        $(wrap).width(winWd);
        $(wrap).height(winHt);

        $(wrap).append(dlog);
        setHandlers();
        return wrap;
    }

    function generateDialogbox() {
        var header = generateHeader();
        var body = generateBody();

        var wrap = global.wrapper = document.createElement("div");
        $(wrap).addClass('sso-notify-wrap animated bounceIn');
        $(wrap).append(header);
        $(wrap).append(body);
        return wrap;
    }

    function generateHeader() {
        var icon = document.createElement("button");

        var title = document.createElement("button");
        $(title).addClass('sso-base-font');
        $(title).html(global.title);

        var wrap = document.createElement("div");
        $(wrap).addClass('sso-notify-header');
        $(wrap).append(icon);
        $(wrap).append(title);
        return wrap;
    }

    function generateBody() {
        var label = global.messageLabel = document.createElement("button");
        $(label).addClass('sso-base-font');
        $(label).html(global.msg);

        var confirm = global.okButton = document.createElement("button");
        $(confirm).addClass('sso-control-button');

        var wrap = document.createElement("div");
        $(wrap).addClass('sso-notify-body');
        $(wrap).append(label);
        $(wrap).append(confirm);
        return wrap;
    }

    function setHandlers() {
        $(global.closeBtn).on('click tap', function() {
            var glass = global.glassWrapper;
            $(glass).addClass('noti-hide');
            $(glass).removeClass('animated bounceIn');
        });
    }

    SsoNotification.prototype.show = function() {
        var glass = global.glassWrapper;
        var postObj = {
            left: 0,
            top: 0
        };

        $(glass).removeClass('noti-hide');
        $(glass).addClass('animated fadeIn');
        $(glass).offset(postObj);
    }

    SsoNotification.prototype.hide = function() {
        var glass = global.glassWrapper;
        $(glass).addClass('noti-hide');
    }

    SsoNotification.prototype.setMessage = function(message) {
        $(global.messageLabel).html(message);
    }

    SsoNotification.prototype.getOkButton = function() {
        return global.okButton;
    }

    SsoNotification.prototype.asWidget = function() {
        return global.glassWrapper;
    }
}

var SsoQuestion = function(dialogTitle, message) {
    if (!(this instanceof SsoQuestion)) {
        throw new Error("This needs to be called with the new keyword");
    }

    var global = {
        title: dialogTitle,
        msg: message,

        okButton: null,
        cancelButton: null,
        messageLabel: null,
        wrapper: null,
        glassWrapper: null,
    };

    global.glassWrapper = generateWidget(); /* Instantiate Widget */

    function generateWidget() {
        var winWd = $(window).width();
        var winHt = $(window).height();

        var dlog = generateDialogbox();

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-noti-wrap noti-hide');
        $(wrap).width(winWd);
        $(wrap).height(winHt);

        $(wrap).append(dlog);
        setHandlers();
        return wrap;
    }

    function generateDialogbox() {
        var header = generateHeader();
        var body = generateBody();

        var wrap = global.wrapper = document.createElement("div");
        $(wrap).addClass('sso-notify-wrap animated bounceIn');
        $(wrap).append(header);
        $(wrap).append(body);
        return wrap;
    }

    function generateHeader() {
        var icon = document.createElement("button");

        var title = document.createElement("button");
        $(title).addClass('sso-base-font');
        $(title).html(global.title);

        var wrap = document.createElement("div");
        $(wrap).addClass('sso-confirm-header');
        $(wrap).append(icon);
        $(wrap).append(title);
        return wrap;
    }

    function generateBody() {
        var label = global.messageLabel = document.createElement("button");
        $(label).addClass('sso-base-font');
        $(label).html(global.msg);

        var confirm = global.okButton = document.createElement("button");
        $(confirm).addClass('sso-control-button sso-dialog-left');

        var cancel = global.cancelButton = document.createElement("button");
        $(cancel).addClass('sso-control-button sso-dialog-right');

        var wrap = document.createElement("div");
        $(wrap).addClass('sso-notify-body');
        $(wrap).append(label);
        $(wrap).append(confirm);
        $(wrap).append(cancel);
        return wrap;
    }

    function setHandlers() {
        $(global.closeBtn).on('click tap', function() {
            var glass = global.glassWrapper;
            $(glass).addClass('noti-hide');
            $(glass).removeClass('animated bounceIn');
        });
    }

    SsoQuestion.prototype.show = function() {
        var glass = global.glassWrapper;
        var postObj = {
            left: 0,
            top: 0
        };

        $(glass).removeClass('noti-hide');
        $(glass).addClass('animated fadeIn');
        $(glass).offset(postObj);
        $(glass).removeClass('sso-noti-wrap');
        $(glass).addClass('sso-noti-wrap');
    }

    SsoQuestion.prototype.hide = function() {
        var glass = global.glassWrapper;
        $(glass).addClass('noti-hide');
    }

    SsoQuestion.prototype.setMessage = function(message) {
        $(global.messageLabel).html(message);
    }

    SsoQuestion.prototype.getOkButton = function() {
        return global.okButton;
    }

    SsoQuestion.prototype.getCancelButton = function() {
        return global.cancelButton;
    }

    SsoQuestion.prototype.asWidget = function() {
        return global.glassWrapper;
    }
}

var SsoLoading = function(logoUrl, dialogTitle) {
    if (!(this instanceof SsoLoading)) {
        throw new Error("This needs to be called with the new keyword");
    }

    var global = {
        title: dialogTitle,
        url: logoUrl,

        messageLabel: null,
        wrapper: null,
        glassWrapper: null,
    };

    global.glassWrapper = generateWidget(); /* Instantiate Widget */

    function generateWidget() {
        var winWd = $(window).width();
        var winHt = $(window).height();

        var dlog = generateDialogbox();

        var wrap = document.createElement('div');
        $(wrap).addClass('sso-noti-wrap');
        $(wrap).width(winWd);
        $(wrap).height(winHt);

        $(wrap).append(dlog);
        return wrap;
    }

    function generateDialogbox() {
        var logo = document.createElement("img");
        $(logo).attr('src', global.url);
        $(logo).addClass('animated fadeIn');

        var icon = document.createElement("button");
        $(icon).addClass('animated bounceIn');

        var title = global.messageLabel = document.createElement("button");
        $(title).addClass('sso-base-font');
        $(title).html(global.title);

        var wrap = global.wrapper = document.createElement("div");
        $(wrap).addClass('sso-load-wrap sso-base-bg ');
        $(wrap).append(logo);
        $(wrap).append(icon);
        $(wrap).append(title);
        return wrap;
    }

    SsoLoading.prototype.show = function() {
        var glass = global.glassWrapper;
        var postObj = {
            left: 0,
            top: 0
        };

        $(glass).removeClass('noti-hide');
        $(glass).addClass('animated fadeIn');
        $(glass).offset(postObj);
        $(glass).removeClass('sso-noti-wrap');
        $(glass).addClass('sso-noti-wrap');
    }

    SsoLoading.prototype.hide = function() {
        var glass = global.glassWrapper;

        $(glass).addClass('animated fadeOut');

        setTimeout(function() {
            $(glass).addClass('noti-hide');
        }, 500);

    }

    SsoLoading.prototype.setMessage = function(message) {
        $(global.messageLabel).html(message);
    }

    SsoLoading.prototype.asWidget = function() {
        return global.glassWrapper;
    }
}