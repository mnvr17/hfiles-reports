/* 
*InformationBox
*SuccessBox
*/
var InformationBox = function(){
	var global = {
		glassWrapper: null,
		boxBody: null,
		iconPnl: null,
		msgTitle: null,
		msgPnl: null,
		okBtn: null,
		closeBtn: null
	}

	initializeWdgt();

	function initializeWdgt(){
		var glassWrapper = global.glassWrapper = document.createElement("div");
		$(glassWrapper).addClass("glassWrapper");

		var boxBody = global.boxBody =document.createElement("div");
		$(boxBody).addClass("boxBody");

		var headerPnl = document.createElement("div");

		var topPnl = document.createElement("div");

		var bottomPnl = document.createElement("div");
		$(bottomPnl).addClass("info-btnPnl");

		var iconPnl = global.iconPnl = document.createElement("div");
		$(iconPnl).addClass("iconPnl info-iconPnl");

		var msgTitle = global.msgTitle = document.createElement("div");
		$(msgTitle).addClass("msgTitle info-msgTitle alertFont");
		$(msgTitle).text("Information");

		var msgPnl = global.msgPnl = document.createElement("div");
		$(msgPnl).addClass("msgPnl alertFont");
	
		var closeBtn = global.closeBtn = document.createElement("button");
		$(closeBtn).addClass("closeBtn info-closeBtn alertFont");
		$(closeBtn).text("x");

		var okBtn = global.okBtn = document.createElement("button");
		$(okBtn).addClass("okBtn info-okBtn alertFont");
		$(okBtn).text("OK");

		//$(headerPnl).append(closeBtn);
		$(topPnl).append(iconPnl);
		$(topPnl).append(msgTitle);
		$(topPnl).append(msgPnl);

		$(bottomPnl).append(okBtn);
		$(boxBody).append(headerPnl);
		$(boxBody).append(topPnl);
		$(boxBody).append(bottomPnl);
		$(glassWrapper).append(boxBody);

		setWdgtHandlers();
	}

	function setWdgtHandlers(){
		$(global.closeBtn).on("click tap",function(){
			hide();
		});

		$(global.okBtn).on("click tap",function(){
			hide();
		});
	}

	function hide(){
		$(global.boxBody).addClass("animated bounceOut");

		setTimeout(function() {
			$(global.glassWrapper).hide();
			$(global.boxBody).removeClass("animated bounceOut");
		}, 600);
	}

	InformationBox.prototype.show = function(msg){
		$(global.glassWrapper).fadeIn();
		$(global.msgPnl).text(msg);

		// $(global.boxBody).addClass("animated bounceIn");
		$(global.iconPnl).addClass("animated flipInX");
		setTimeout(function() {
			// $(global.boxBody).removeClass("animated bounceIn");
			$(global.iconPnl).removeClass("animated flipInX");
		}, 600);
	}

	InformationBox.prototype.close = function() {
		hide();
	}

	InformationBox.prototype.setTitle = function(title){
		$(global.msgTitle).text(title);
	}

	InformationBox.prototype.setMessage = function(msg){
		$(global.msgPnl).text(msg);
	}

	InformationBox.prototype.getWidget = function(){
		return global.glassWrapper;
	}
}

/****Success alert widget****/
var SuccessBox = function(){
	var global = {
		glassWrapper: null,
		boxBody: null,
		msgTitle: null,
		iconPnl: null,
		msgPnl: null,
		okBtn: null,
		closeBtn: null
	}

	initializeWdgt();

	function initializeWdgt(){
		var glassWrapper = global.glassWrapper = document.createElement("div");
		$(glassWrapper).addClass("glassWrapper");

		var boxBody = global.boxBody =document.createElement("div");
		$(boxBody).addClass("boxBody");

		var headerPnl = document.createElement("div");

		var topPnl = document.createElement("div");

		var bottomPnl = document.createElement("div");
		$(bottomPnl).addClass("success-btnPnl");

		var iconPnl = global.iconPnl = document.createElement("div");
		$(iconPnl).addClass("iconPnl success-iconPnl");

		var msgTitle = global.msgTitle = document.createElement("div");
		$(msgTitle).addClass("msgTitle success-msgTitle alertFont");
		$(msgTitle).text("Successful");

		var msgPnl = global.msgPnl = document.createElement("div");
		$(msgPnl).addClass("msgPnl alertFont");
	
		var closeBtn = global.closeBtn = document.createElement("button");
		$(closeBtn).addClass("closeBtn success-closeBtn alertFont");
		$(closeBtn).text("x");

		var okBtn = global.okBtn = document.createElement("button");
		$(okBtn).addClass("okBtn success-okBtn alertFont");
		$(okBtn).text("OK");

		//$(headerPnl).append(closeBtn);
		$(topPnl).append(iconPnl);
		$(topPnl).append(msgTitle);
		$(topPnl).append(msgPnl);

		$(bottomPnl).append(okBtn);
		$(boxBody).append(headerPnl);
		$(boxBody).append(topPnl);
		$(boxBody).append(bottomPnl);
		$(glassWrapper).append(boxBody);

		setWdgtHandlers();
	}

	function setWdgtHandlers(){
		$(global.closeBtn).on("click tap",function(){
			hide();
		});

		$(global.okBtn).on("click tap",function(){
			hide();
		});
	}

	function hide(){
		$(global.boxBody).addClass("animated bounceOut");
		
		setTimeout(function() {
			$(global.glassWrapper).hide();
			$(global.boxBody).removeClass("animated bounceOut");
		}, 600);
	}

	SuccessBox.prototype.show = function(msg){
		$(global.glassWrapper).fadeIn();
		$(global.msgPnl).text(msg);

		// $(global.boxBody).addClass("animated bounceIn");
		$(global.iconPnl).addClass("animated flipInX");
		setTimeout(function() {
			// $(global.boxBody).removeClass("animated bounceIn");
			$(global.iconPnl).removeClass("animated flipInX");
		}, 1000);
	}

	SuccessBox.prototype.close = function() {
		hide();
	}

	SuccessBox.prototype.setTitle = function(title){
		$(global.msgTitle).text(title);
	}

	SuccessBox.prototype.setMessage = function(msg){
		$(global.msgPnl).text(msg);
	}

	SuccessBox.prototype.getWidget = function(){
		return global.glassWrapper;
	}
}

/****Error alert widget****/
var ErrorBox = function(){
	var global = {
		glassWrapper: null,
		boxBody: null,
		msgTitle: null,
		iconPnl: null,
		msgPnl: null,
		okBtn: null,
		closeBtn: null
	}

	initializeWdgt();

	function initializeWdgt(){
		var glassWrapper = global.glassWrapper = document.createElement("div");
		$(glassWrapper).addClass("glassWrapper");

		var boxBody = global.boxBody =document.createElement("div");
		$(boxBody).addClass("boxBody");

		var headerPnl = document.createElement("div");

		var topPnl = document.createElement("div");

		var bottomPnl = document.createElement("div");
		$(bottomPnl).addClass("error-btnPnl");

		var iconPnl = global.iconPnl = document.createElement("div");
		$(iconPnl).addClass("iconPnl error-iconPnl");

		var msgTitle = global.msgTitle = document.createElement("div");
		$(msgTitle).addClass("msgTitle error-msgTitle alertFont");
		$(msgTitle).text("Error");

		var msgPnl = global.msgPnl = document.createElement("div");
		$(msgPnl).addClass("msgPnl alertFont");
	
		var closeBtn = global.closeBtn = document.createElement("button");
		$(closeBtn).addClass("closeBtn error-closeBtn alertFont");
		$(closeBtn).text("x");

		var okBtn = global.okBtn = document.createElement("button");
		$(okBtn).addClass("okBtn error-okBtn alertFont");
		$(okBtn).text("OK");

		//$(headerPnl).append(closeBtn);
		$(topPnl).append(iconPnl);
		$(topPnl).append(msgTitle);
		$(topPnl).append(msgPnl);

		$(bottomPnl).append(okBtn);
		$(boxBody).append(headerPnl);
		$(boxBody).append(topPnl);
		$(boxBody).append(bottomPnl);
		$(glassWrapper).append(boxBody);

		setWdgtHandlers();
	}

	function setWdgtHandlers(){
		$(global.closeBtn).on("click tap",function(){
			hide();
		});

		$(global.okBtn).on("click tap",function(){
			hide();
		});
	}

	function hide(){
		$(global.boxBody).addClass("animated bounceOut");
		
		setTimeout(function() {
			$(global.glassWrapper).hide();
			$(global.boxBody).removeClass("animated bounceOut");
		}, 600);
	}

	ErrorBox.prototype.show = function(msg){
		$(global.glassWrapper).fadeIn();
		$(global.msgPnl).text(msg);

		$(global.iconPnl).addClass("animated flipInX");
		setTimeout(function() {
			$(global.iconPnl).removeClass("animated flipInX");
		}, 1000);
	}

	ErrorBox.prototype.close = function() {
		hide();
	}

	ErrorBox.prototype.setTitle = function(title){
		$(global.msgTitle).text(title);
	}

	ErrorBox.prototype.setMessage = function(msg){
		$(global.msgPnl).text(msg);
	}

	ErrorBox.prototype.getWidget = function(){
		return global.glassWrapper;
	}
}

/**** Warning alert widget ****/
var WarningBox = function(){
	var global = {
		glassWrapper: null,
		boxBody: null,
		msgTitle: null,
		iconPnl: null,
		msgPnl: null,
		okBtn: null,
		closeBtn: null
	}

	initializeWdgt();

	function initializeWdgt(){
		var glassWrapper = global.glassWrapper = document.createElement("div");
		$(glassWrapper).addClass("glassWrapper");

		var boxBody = global.boxBody =document.createElement("div");
		$(boxBody).addClass("boxBody");

		var headerPnl = document.createElement("div");

		var topPnl = document.createElement("div");

		var bottomPnl = document.createElement("div");
		$(bottomPnl).addClass("warning-btnPnl");

		var iconPnl = global.iconPnl = document.createElement("div");
		$(iconPnl).addClass("iconPnl warning-iconPnl");

		var msgTitle = global.msgTitle = document.createElement("div");
		$(msgTitle).addClass("msgTitle warning-msgTitle alertFont");
		$(msgTitle).text("Warning");

		var msgPnl = global.msgPnl = document.createElement("div");
		$(msgPnl).addClass("msgPnl alertFont");
	
		var closeBtn = global.closeBtn = document.createElement("button");
		$(closeBtn).addClass("closeBtn warning-closeBtn alertFont");
		$(closeBtn).text("x");

		var okBtn = global.okBtn = document.createElement("button");
		$(okBtn).addClass("okBtn warning-okBtn alertFont");
		$(okBtn).text("OK");

		//$(headerPnl).append(closeBtn);
		$(topPnl).append(iconPnl);
		$(topPnl).append(msgTitle);
		$(topPnl).append(msgPnl);

		$(bottomPnl).append(okBtn);
		$(boxBody).append(headerPnl);
		$(boxBody).append(topPnl);
		$(boxBody).append(bottomPnl);
		$(glassWrapper).append(boxBody);

		setWdgtHandlers();
	}

	function setWdgtHandlers(){
		$(global.closeBtn).on("click tap",function(){
			hide();
		});

		$(global.okBtn).on("click tap",function(){
			hide();
		});
	}

	function hide(){
		$(global.boxBody).addClass("animated bounceOut");
		
		setTimeout(function() {
			$(global.glassWrapper).hide();
			$(global.boxBody).removeClass("animated bounceOut");
		}, 600);
	}

	WarningBox.prototype.show = function(msg){
		$(global.glassWrapper).fadeIn();
		$(global.msgPnl).text(msg);

		$(global.iconPnl).addClass("animated flipInX");
		setTimeout(function() {
			$(global.iconPnl).removeClass("animated flipInX");
		}, 1000);
	}

	WarningBox.prototype.close = function() {
		hide();
	}

	WarningBox.prototype.setTitle = function(title){
		$(global.msgTitle).text(title);
	}

	WarningBox.prototype.setMessage = function(msg){
		$(global.msgPnl).text(msg);
	}

	WarningBox.prototype.getWidget = function(){
		return global.glassWrapper;
	}
}

/**** Confirmation alert widget ****/
var ConfirmationBox = function(){
	var global = {
		glassWrapper: null,
		boxBody: null,
		msgTitle: null,
		iconPnl: null,
		msgPnl: null,
		okBtn: null,
		noBtn: null,
		closeBtn: null
	}

	initializeWdgt();

	function initializeWdgt(){
		var glassWrapper = global.glassWrapper = document.createElement("div");
		$(glassWrapper).addClass("glassWrapper");

		var boxBody = global.boxBody =document.createElement("div");
		$(boxBody).addClass("boxBody");

		var headerPnl = document.createElement("div");

		var topPnl = document.createElement("div");

		var bottomPnl = document.createElement("div");
		$(bottomPnl).addClass("confirm-btnPnl");

		var iconPnl = global.iconPnl = document.createElement("div");
		$(iconPnl).addClass("iconPnl confirm-iconPnl");

		var msgTitle = global.msgTitle = document.createElement("div");
		$(msgTitle).addClass("msgTitle confirm-msgTitle alertFont");
		$(msgTitle).text("Confirmation");

		var msgPnl = global.msgPnl = document.createElement("div");
		$(msgPnl).addClass("msgPnl alertFont");
	
		var closeBtn = global.closeBtn = document.createElement("button");
		$(closeBtn).addClass("closeBtn confirm-closeBtn alertFont");
		$(closeBtn).text("x");

		var okBtn = global.okBtn = document.createElement("button");
		$(okBtn).addClass("yesBtn confirm-okBtn alertFont");
		$(okBtn).text("Yes");

		var noBtn = global.noBtn = document.createElement("button");
		$(noBtn).addClass("noBtn confirm-noBtn alertFont");
		$(noBtn).text("No");

		//$(headerPnl).append(closeBtn);
		$(topPnl).append(iconPnl);
		$(topPnl).append(msgTitle);
		$(topPnl).append(msgPnl);

		$(bottomPnl).append(noBtn);
		$(bottomPnl).append(okBtn);
		$(boxBody).append(headerPnl);
		$(boxBody).append(topPnl);
		$(boxBody).append(bottomPnl);
		$(glassWrapper).append(boxBody);

		setWdgtHandlers();
	}

	function setWdgtHandlers(){
		$(global.closeBtn).on("click tap",function(){
			hide();
		});

		$(global.okBtn).on("click tap",function(){
			hide();
		});
	}

	function hide(){
		$(global.boxBody).addClass("animated bounceOut");
		
		setTimeout(function() {
			$(global.glassWrapper).hide();
			$(global.boxBody).removeClass("animated bounceOut");
		}, 600);
	}

	ConfirmationBox.prototype.show = function(msg){
		$(global.glassWrapper).fadeIn();
		$(global.msgPnl).text(msg);

		$(global.iconPnl).addClass("animated flipInX");
		setTimeout(function() {
			$(global.iconPnl).removeClass("animated flipInX");
		}, 1000);
	}

	ConfirmationBox.prototype.close = function() {
		hide();
	}

	ConfirmationBox.prototype.setTitle = function(title){
		$(global.msgTitle).text(title);
	}

	ConfirmationBox.prototype.setMessage = function(msg){
		$(global.msgPnl).text(msg);
	}

	ConfirmationBox.prototype.getWidget = function(){
		return global.glassWrapper;
	}
}