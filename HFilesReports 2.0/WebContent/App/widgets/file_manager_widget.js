var FileManager = function() {
	var global = {
		explorer: null,
		fileSummary: null,
			
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var explorer = global.explorer = $.extend(true, {}, new Explorer());
		explorer.populate();
		
		var fileSummary = global.fileSummary = $.extend(true, {}, new FileSummary());
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("file-manager");
		$(wrapper).append(explorer.getWidget());
		$(wrapper).append(fileSummary.getWidget());
	}
	
	FileManager.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	FileManager.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	
	FileManager.prototype.getExplorer = function() {
		return global.explorer;
	}
	
	FileManager.prototype.getFileSummary = function() {
		return global.fileSummary;
	}
	
	FileManager.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var Explorer = function() {
	var global = {
		company: null,
		folders: null,
		mainFolderWidgets: [],
		subFolderWidgets: [],
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var company = global.company = $.extend(true, {}, new Select());
		company.setLabelText("Select company");
		company.addStyle("file-explorer-company");
		
		var folders = global.folders = document.createElement("div");
		$(folders).addClass("file-explorer-folders");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("file-explorer");
		$(wrapper).append(company.getElement());
		$(wrapper).append(folders);
	}
	
	function populate() {
		clearValues();
		
		for (var mainFolderName in fileExplorerConfig) {
			var mainFolder = $.extend(true, {}, new MainFolder());
			mainFolder.getFolder().setFolderName(mainFolderName);
			
			var subFolderNames = fileExplorerConfig[mainFolderName];
			for (var i = 0; i < subFolderNames.length; i++) {
				var subFolderName = subFolderNames[i];
				var subFolder = $.extend(true, {}, new SubFolder());
				subFolder.getFolder().setFolderName(subFolderName);
				$(subFolder.getWidget()).data("frequency", subFolderName);
				$(subFolder.getWidget()).data("reportType", mainFolderName);
				
				$(mainFolder.getSubFolders()).append(subFolder.getWidget());
				global.subFolderWidgets.push(subFolder);
			}
			$(global.folders).append(mainFolder.getWidget());
			global.mainFolderWidgets.push(mainFolder);
		}
	}
	
	function clearValues() {
		$(global.folders).empty();
		folderWidget = [];
	}
	
	Explorer.prototype.getCompany = function() {
		return global.company;
	}
	
	Explorer.prototype.getMainFolderWidgets = function() {
		return global.mainFolderWidgets;
	}
	
	Explorer.prototype.getSubFolderWidgets = function() {
		return global.subFolderWidgets;
	}
	 
	Explorer.prototype.populate = function() {
		populate();
	}
	
	Explorer.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var MainFolder = function() {
	var global = {
		folder: null,
		subFolders: null,
		
		wrapper: null	
	};
	
	initialize();
	
	function initialize() {
		var folder = global.folder = $.extend(true, {}, new Folder());
		
		var subFolders = global.subFolders = document.createElement("ul");
		$(subFolders).addClass("file-explorer-subFolders");
		
		var wrapper = global.wrapper = document.createElement("ul");
		$(wrapper).addClass("file-explorer-mainFolder");
		$(wrapper).append(folder.getWidget);
		$(wrapper).append(subFolders);
	}
	
	MainFolder.prototype.getSubFolders = function() {
		return global.subFolders;
	}
	
	MainFolder.prototype.getFolder = function() {
		return global.folder;
	}
	
	MainFolder.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var SubFolder = function() {
	var global = {
		folder: null,
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var folder = global.folder = $.extend(true, {}, new Folder());
		
		var wrapper = global.wrapper = document.createElement("li");
		$(wrapper).addClass("file-explorer-subFolder");
		$(wrapper).append(folder.getWidget);
	}
	
	SubFolder.prototype.getFolder = function() {
		return global.folder;
	}
	
	SubFolder.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var Folder = function() {
	var global = {
		folderIcon: null,
		folderName: null,
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var folderIcon = global.folderIcon = document.createElement("div");
		
		var folderName = global.folderName = document.createElement("button");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("file-explorer-folder");
		$(wrapper).append(folderIcon);
		$(wrapper).append(folderName);
	}
	
	Folder.prototype.setFolderName = function(folderName) {
		$(global.folderName).text(folderName);
	}
	
	Folder.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var FileSummary = function() {
	var global = {
		files: [],
		header: null,
		summary: null,
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var header = global.header = document.createElement("button");
		$(header).addClass("file-summary-header");
		
		var summary = global.summary = document.createElement("div");
		$(summary).addClass("file-summary-content");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("file-summary");
		$(wrapper).append(header);
		$(wrapper).append(summary);
	}
	
	function noFilesFound() {
		var message = "No files found";
		var messageElement = document.createElement("div");
		$(messageElement).addClass("file-summary-empty");
		$(messageElement).text(message);
		$(global.summary).append(messageElement);
	}
	
	function populate(filesData) {
		clearValues();
		
		if (filesData) {
			for (var i in filesData) {
				var fileData = filesData[i];
				addFile(fileData);
			}
		}
	}
	
	function clearValues() {
		$(global.header).text("");
		$(global.summary).empty();
		global.files = [];
	}
	
	function addFile(fileData) {
		if (fileData) {
			var file = $.extend(true, {}, new File());
			var name = fileData.name;

			file.setFileIcon(getFileIcon(getFileType(name)));
			file.setFileName(name);
			file.setFileType(getFileType(name));
			file.setFileId(fileData.fileId);
			file.setGfsKey(fileData.gfsKey);
			
			$(global.summary).append(file.getWidget());
			global.files.push(file);
		}
	}
	
	function getFileType(fileName) {
		var split = fileName.split(".").pop();
		return split;
	}
	
	function getFileIcon(fileType) {
		var iconPath = "App/assets/img/file_types/" + fileType.toLowerCase() + "_file.png";
		return "url(" + iconPath + ")";
	}
	
	function generateHeader(reportType, frequency) {
		return reportType + " > " + frequency;
	}
	
	FileSummary.prototype.noFilesFound = function() {
		noFilesFound();
	}
	
	FileSummary.prototype.clearValues = function() {
		clearValues();
	}
	
	FileSummary.prototype.getFiles = function() {
		return global.files;
	}
	
	FileSummary.prototype.getHeader = function() {
		return $(global.header).text();
	}
	
	FileSummary.prototype.setHeader = function(reportType, frequency) {
		$(global.header).text(generateHeader(reportType, frequency));
	}
	
	FileSummary.prototype.populate = function(filesData) {
		populate(filesData);
	}
	
	FileSummary.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var File = function() {
	var global = {

		fileIcon: null,
		fileName: null,
		fileType: null,
		fileId: null,
		gfsKey: null,
		viewBtn: null,
		downloadBtn: null,
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var fileIcon = global.fileIcon = document.createElement("div");
		
		var fileName = global.fileName = document.createElement("button");
		
		var viewBtn = global.viewBtn = document.createElement("button");
		
		var downloadBtn = global.downloadBtn = document.createElement("button");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("file-summary-file");
		$(wrapper).append(fileIcon);
		$(wrapper).append(fileName);
		$(wrapper).append(viewBtn);
		$(wrapper).append(downloadBtn);
	}
	
	File.prototype.setFileIcon = function(icon) {
		$(global.fileIcon).css("background-image", icon);
	}
	
	File.prototype.setFileName = function(fileName) {
		$(global.fileName).text(fileName);
	}
	
	File.prototype.setFileType = function(fileType) {
		global.fileType = fileType;
	}
	
	File.prototype.setFileId = function(fileId) {
		global.fileId = fileId;
	}
	
	File.prototype.setGfsKey = function(gfsKey) {
		global.gfsKey = gfsKey;
	}
	
	File.prototype.getFileName = function() {
		return $(global.fileName).text();
	}
	
	File.prototype.getFileType = function() {
		return global.fileType;
	}
	
	File.prototype.getFileId = function() {
		return global.fileId;
	}
	
	File.prototype.getGfsKey = function() {
		return global.gfsKey;
	}
	
	File.prototype.getDownloadBtn = function() {
		return global.downloadBtn;
	}
	
	File.prototype.getViewBtn = function() {
		return global.viewBtn;
	}
	
	File.prototype.getWidget = function() {
		return global.wrapper;
	}
}