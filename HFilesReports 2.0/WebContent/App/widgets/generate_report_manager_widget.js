var GenerateReportManager = function() {
	var global = {
		titleHeader: null,
		contentWrapper: null,
		genRepExplorer: null,
			
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var header = $.extend(true, {}, new TitleHeader());
		$(header.getWidget()).addClass("title-header");
		
		var utilHeader = document.createElement("div");
		$(utilHeader).addClass("title-header-pnl");
		$(utilHeader).append(header.getWidget());
		
		var contentWrapper = global.contentWrapper = document.createElement("div");
		$(contentWrapper).addClass("content-wrapper");
		
		var genRepExplorer = global.genRepExplorer = $.extend(true, {}, new GenRepExplorer());
		genRepExplorer.populate();
		
		var fileSummary = global.fileSummary = $.extend(true, {}, new FileSummary());
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("generate-report-manager");
		$(wrapper).append(utilHeader);
		$(wrapper).append(contentWrapper);
		$(wrapper).append(genRepExplorer.getWidget());
		$(wrapper).append(fileSummary.getWidget());
	}
	
	GenerateReportManager.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	GenerateReportManager.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	GenerateReportManager.prototype.getGenRepExplorer = function() {
		return global.genRepExplorer;
	}
	
	GenerateReportManager.prototype.getFileSummary = function() {
		return global.fileSummary;
	}
	
	GenerateReportManager.prototype.getWidget = function() {
		return global.wrapper;
	}
	
	GenerateReportManager.prototype.getContentWrapper = function() {
		return global.contentWrapper;
	}
}

var TitleHeader = function() {
	var global = {
		titleText: "TITLE HEADER",
		descriptionText: "",
		
		title: null,
		description: null,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var title = global.title = document.createElement("div");
		$(title).text(global.titleText);
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(title);
	}
	
	TitleHeader.prototype.setTitle = function(title) {
		global.titleText = title;
		$(global.title).text(global.titleText);
	}
	
	TitleHeader.prototype.setDescription = function(description) {
		global.descriptionText = description;
		$(global.description).text(global.descriptionText);
	}
	
	TitleHeader.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var GenRepExplorer = function() {
	var global = {
		company: null,
		reportType: null,
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var company = global.company = $.extend(true, {}, new Select());
		company.setLabelText("Select company");
		company.addStyle("file-explorer-company");
		
		var reportType = global.reportType = $.extend(true, {}, new Select());
		reportType.setLabelText("Select report type");
		reportType.addStyle("file-explorer-company");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("file-explorer");
		$(wrapper).append(company.getElement());
		$(wrapper).append(reportType.getElement());
		
	}
	
	function populate() {
		
		
	}
	
	GenRepExplorer.prototype.getCompany = function() {
		return global.company;
	}
	 
	GenRepExplorer.prototype.populate = function() {
		populate();
	}
	
	GenRepExplorer.prototype.getWidget = function() {
		return global.wrapper;
	}
}

