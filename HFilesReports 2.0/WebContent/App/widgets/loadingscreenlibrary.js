var LoadingScreenWidget = function() {
	
	var setting = {
		overlay: null,
		hiddenContainer: null,
		loaderContainer: null,
		loader: null,
		loaderMsg: null
	}
	
	initialize();
	
	function initialize() {
		setting.hiddenContainer = document.createElement("div");
		$(setting.hiddenContainer).addClass("loader-hiddenContainer");
		
		setting.loaderContainer = document.createElement("div");
		$(setting.loaderContainer).addClass("loader-loaderContainer");
		
		setting.loader = document.createElement("div");
		$(setting.loader).addClass("loader-loader");
		
		setting.loaderMsg = document.createElement("div");
		$(setting.loaderMsg).html("uploading");
		$(setting.loaderMsg).addClass("loader-loaderMsg");
		
		setting.overlay = document.createElement("div");
		$(setting.overlay).addClass("overlay");
		
		$(setting.overlay).append(setting.hiddenContainer);
		$(setting.hiddenContainer).append(setting.loaderContainer);
		$(setting.loaderContainer).append(setting.loader);
		$(setting.loaderContainer).append(setting.loaderMsg);
	}
	
	LoadingScreenWidget.prototype.showLoader = function(loaderMsg) {
		$(setting.loaderMsg).html(loaderMsg);
		$(setting.overlay).show();
		$(setting.hiddenContainer).show();
	}
	
	LoadingScreenWidget.prototype.hideLoader = function() {
		$(setting.loaderMsg).html("");
		$(setting.overlay).hide();
		$(setting.hiddenContainer).hide();
	}
	
	LoadingScreenWidget.prototype.getWidget = function() {
		return setting.overlay;
	}
}