var ReportViewer = function () {
	var global = {
		wrapper: null,
		viewer: null,
		
		reportType: null,
		reportTitle: null,
		
		reportTypeElem: null,
		reportTitleElem: null
	};
	
	initialize();
	
	function initialize() {
		var viewerWrapper = document.createElement("div");
		$(viewerWrapper).append(buildViewerHeader());
		$(viewerWrapper).append(buildViewerContent());
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("reportViewer");
		$(wrapper).append(viewerWrapper);
	}
	
	function buildViewerHeader() {
		var reportType = global.reportTypeElem = document.createElement("button");
		$(reportType).text(global.reportType);
		
		var reportTitle = global.reportTitleElem = document.createElement("button");
		$(reportTitle).text(global.reportTitle);

		var headerTxtPnl = document.createElement("div");
		$(headerTxtPnl).addClass("viewerHeaderTxt");
		$(headerTxtPnl).append(reportType);
		$(headerTxtPnl).append(reportTitle);
		
		var closeBtn = document.createElement("button");
		$(closeBtn).text("Close");
		$(closeBtn).on("click tap", hideViewer);
		
		var btnPnl = document.createElement("div");
		$(btnPnl).addClass("viewerHeaderBtn");
		$(btnPnl).append(closeBtn);
		
		var header = document.createElement("div");
		$(header).addClass("viewerHeader");
		$(header).append(headerTxtPnl);
		$(header).append(btnPnl);
		
		return header;
	}
	
	function buildViewerContent() {
		var viewer = global.viewer = document.createElement("iframe");
		var viewerPnl = document.createElement("div");
		$(viewerPnl).addClass("viewerContent");
		$(viewerPnl).append(viewer);
		
		return viewerPnl;
	}
	
	function hideViewer() {
		$(global.viewer).attr("src", "");
		$(global.wrapper).fadeOut("fast");
	}
	
	function showViewer(type, frequency) {
		global.reportType = type;
		$(global.reportTypeElem).text(global.reportType);
		
		global.reportTitle = frequency;
		$(global.reportTitleElem).text(global.reportTitle);
		
		$(global.wrapper).fadeIn("fast");
	}
	
	ReportViewer.prototype.hideViewer = function() {
		hideViewer();
	}
	
	ReportViewer.prototype.showViewer = function(type, frequency) {
		showViewer(type, frequency);
	}
	
	ReportViewer.prototype.setViewerUrl = function(url) {
		$(global.viewer).attr("src", url);
	}
	
	ReportViewer.prototype.getWidget = function() {
		return global.wrapper;
	}
}