var Reports = function() {
	var global = {
		wrapper: null,
		menuWrapper: null,
		contentWrapper: null,
		
		menuBtns: [],
	};
	
	initialize();
	
	function initialize() {
		var menuWrapper = global.menuWrapper = document.createElement("div");
		$(menuWrapper).addClass("menu");
		
		var contentWrapper = global.contentWrapper = document.createElement("div");
		$(contentWrapper).addClass("content");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("reports");
		$(wrapper).append(menuWrapper);
		$(wrapper).append(contentWrapper);
		
		buildMenu();
	}
	
	function buildMenu() {
		for (var key in menuConfig) {
			var menu = menuConfig[key];
			var idx = menu.index;
			var label = menu.label;
			var menuBtn = document.createElement("button");
			$(menuBtn).addClass("menuBtn");
			$(menuBtn).data("type", "menu");
			$(menuBtn).data("idx", idx);
			$(menuBtn).data("key", key);
			$(menuBtn).text(label);
			$(global.menuWrapper).append(menuBtn);
			
			global.menuBtns.push(menuBtn);
		}
	}
	
	function selectMenu(idx) {
		resetSelected();
		
		var menu = global.menuBtns[idx];
		$(menu).addClass("selected");
	}
	
	function resetSelected() {
		for (var i = 0; i < global.menuBtns.length; i++) {
			var menuBtn = global.menuBtns[i];
			$(menuBtn).removeClass("selected");
		}
	}
	
	Reports.prototype.selectMenu = function(idx) {
		selectMenu(idx);
	}
	
	Reports.prototype.getContentWrapper = function() {
		return global.contentWrapper;
	}
	
	Reports.prototype.getMenuWrapper = function() {
		return global.menuWrapper;
	}
	
	Reports.prototype.getWidget = function() {
		return global.wrapper;
	}
}