var Input = function() {
	var global = {
		label: null,
		labelText: "",
		input: null,
		value: "",
		required: false,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var label = global.label = document.createElement("button");
		$(label).text(global.labelText);
		
		var input = global.input = document.createElement("input");
		$(input).val(global.value);
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(label);
		$(wrapper).append(input);
		
		setInputHandler();
	}
	
	function setInputHandler() {
		$(global.input).on("blur", function(e) {
			$(global.select).removeClass("modal-field-invalid");
			global.value = $(this).val();
		});
	}
	
	function isValid() {
		var valid = true;
		
		if (global.required) {
			if (!(global.value.trim())) {
				valid = false;
			} else {
				$(global.select).addClass("modal-field-invalid");
			}
		}
		return valid;
	}
	
	function selectizeField() {
		var $select = $(global.select).selectize({
		    delimiter: ',',
		    persist: false,
		    create: function(input) {
		        return {
		            value: input,
		            text: input
		        }
		    }
		});
		
		return $select[0].selectize;
	}
	
	Input.prototype.selectizeField = function() {
		return selectizeField();
	}
	
	Input.prototype.isValid = function() {
		return isValid();
	}
	
	Input.prototype.required = function() {
		var span = document.createElement("span");
		$(span).append("*");
		$(global.label).append(span);
		
		global.required = true;
	}
	
	Input.prototype.clear = function() {
		global.value = "";
		$(global.input).val(global.value);
	}
	
	Input.prototype.remove = function() {
		$(global.wrapper).remove();
	}
	
	Input.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	Input.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	Input.prototype.enable = function() {
		$(global.input).attr("disabled", false);
	}
	
	Input.prototype.disable = function() {
		$(global.input).attr("disable", true);
	}
	
	Input.prototype.addStyle = function(style) {
		$(global.wrapper).addClass(style);
	}
	
	Input.prototype.removeStyle = function(style) {
		$(global.wrapper).removeClass(style);
	}
	
	Input.prototype.resetStyle = function() {
		$(global.wrapper).removeClass();
	}
	
	Input.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	Input.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	Input.prototype.setValue = function(value) {
		global.value = value;
		$(global.input).val(global.value);
	}
	
	Input.prototype.getValue = function() {
		return global.value;
	}
	
	Input.prototype.setAttribute = function(key, value) {
		$(global.input).attr(key, value);
	}
	
	Input.prototype.getAttribute = function(key) {
		return $(global.input).attr(key);
	}
	
	Input.prototype.setData = function(key, data) {
		$(global.input).data(key, data);
	}
	
	Input.prototype.getData = function(key) {
		return $(global.input).data(key);
	}
	
	Input.prototype.getLabel = function() {
		return global.label;
	}
	
	Input.prototype.getInput = function() {
		return global.input;
	}
	
	Input.prototype.getElement = function() {
		return global.wrapper;
	}
}
	
var Select = function() {
	var global = {
		label: null,
		labelText: "",
		select: null,
		value: "",
		required: false,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var label = global.label = document.createElement("button");
		$(label).text(global.labelText);
		
		var option = document.createElement("option");
		$(option).val("");
		$(option).text("");
		
		var select = global.select = document.createElement("select");
		$(select).append(option);
		$(select).val(global.value);
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(label);
		$(wrapper).append(select);
		
		setSelectHandler();
	}
	
	function setSelectHandler() {
		$(global.select).on("change", function(e) {
			$(global.select).removeClass("modal-field-invalid");
			global.value = $(this).find(":selected").val();
		});
	}
	
	function populate(data) {
		var select = global.select;
		$(select).empty();
		
		var emptyOption = document.createElement("option");
		$(emptyOption).val("");
		$(emptyOption).text("");
		$(select).append(emptyOption);
		
		for (var i = 0; i < data.length; i++) {
			var datum = data[i];
			var text = datum["text"];
			var value = datum["value"];
			var option = document.createElement("option");
			
			$(option).val(value);
			$(option).text(text);
			$(select).append(option);
		}
		
		$(select).val("");
		$(select).trigger("change");
	}
	
	function customPopulate(data) {
		var select = global.select;
		$(select).empty();
		
		var emptyOption = document.createElement("option");
		$(emptyOption).val("");
		$(emptyOption).text("");
		$(select).append(emptyOption);
		
		for (var value in data) {
			var text = data[value];
			var option = document.createElement("option");
			
			$(option).val(value);
			$(option).text(text);
			$(select).append(option);
		}
		$(select).val("");
		$(select).trigger("change");
	}
	
	function isValid() {
		var valid = true;
		
		if (global.required) {
			if (!(global.value.trim())) {
				valid = false;
			} else {
				$(global.select).addClass("modal-field-invalid");
			}
		}
		return valid;
	}
	
	Select.prototype.customPopulate = function(data) {
		customPopulate(data);
	}
	
	Select.prototype.isValid = function() {
		return isValid();
	}
	
	Select.prototype.required = function() {
		var span = document.createElement("span");
		$(span).append("*");
		$(global.label).append(span);
		
		global.required = true;
	}
	
	Select.prototype.clear = function() {
		$(global.select).val("");
		$(global.select).trigger("change");
	}
	
	Select.prototype.empty = function() {
		$(global.select).empty();
		
		var option = document.createElement("option");
		$(option).val("");
		$(option).text("");
		
		$(global.select).append(option);
		$(global.select).trigger("change");
	}
	
	Select.prototype.populate = function(data) {
		populate(data);
	}
	
	Select.prototype.remove = function() {
		$(global.wrapper).remove();
	}
	
	Select.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	Select.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	Select.prototype.triggerEvent = function() {
		$(global.select).trigger("change");
	}
	
	Select.prototype.enable = function() {
		$(global.select).attr("disabled", false);
	}
	
	Select.prototype.disable = function() {
		$(global.select).attr("disable", true);
	}
	
	Select.prototype.addStyle = function(style) {
		$(global.wrapper).addClass(style);
	}
	
	Select.prototype.removeStyle = function(style) {
		$(global.wrapper).removeClass(style);
	}
	
	Select.prototype.resetStyle = function() {
		$(global.wrapper).removeClass();
	}
	
	Select.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	Select.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	Select.prototype.setValue = function(value) {
		global.value = value;
		$(global.select).val(global.value);
		$(global.select).trigger("change");
	}
	
	Select.prototype.getValue = function() {
		return global.value;
	}
	
	Select.prototype.setAttribute = function(key, value) {
		$(global.select).attr(key, value);
	}
	
	Select.prototype.getAttribute = function(key) {
		return $(global.select).attr(key);
	}
	
	Select.prototype.setData = function(key, data) {
		$(global.select).data(key, data);
	}
	
	Select.prototype.getData = function(key) {
		return $(global.select).data(key);
	}
	
	Select.prototype.getLabel = function() {
		return global.label;
	}
	
	Select.prototype.getSelect = function() {
		return global.select;
	}
	
	Select.prototype.getElement = function() {
		return global.wrapper;
	}
}

var Selectize = function() {
	var global = {
		label: null,
		labelText: "",
		select: null,
		selectize: null,
		value: "",
		required: false,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var label = global.label = document.createElement("button");
		$(label).text(global.labelText);
		
		var option = document.createElement("option");
		$(option).val("");
		$(option).text("");
		
		var select = global.select = document.createElement("select");
		$(select).append(option);
		$(select).val(global.value);
		
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(label);
		$(wrapper).append(select);

		setSelectHandler();
	}
	
	function selectizeField(select) {
		var $select = $(select).selectize({
		    delimiter: ',',
		    persist: false,
			maxItems: null
		});
		
		return $select[0].selectize;
	}
	
	function setSelectHandler() {
		$(global.select).on("change", function(e) {
			$(global.select).next("div.selectize-control").removeClass("modal-field-invalid");
			var value = $(this).val()
			if (value) {
				global.value = value.join("|");
			} else {
				global.value = [];
			}
		});
	}
	
	function populate(data) {
		var select = global.select;
		$(select).empty();
		
		var emptyOption = document.createElement("option");
		$(emptyOption).val("");
		$(emptyOption).text("");
		$(select).append(emptyOption);
		
		for (var i = 0; i < data.length; i++) {
			var datum = data[i];
			var text = datum["text"];
			var value = datum["value"];
			var option = document.createElement("option");
			
			$(option).val(value);
			$(option).text(text);
			$(select).append(option);
		}
		
		$(select).val("");
		$(select).trigger("change");
	}
	
	function customPopulate(data) {
		var select = global.select;
		$(select).empty();
		
		var emptyOption = document.createElement("option");
		$(emptyOption).val("");
		$(emptyOption).text("");
		$(select).append(emptyOption);
		
		for (var value in data) {
			var text = data[value];
			var option = document.createElement("option");
			
			$(option).val(value);
			$(option).text(text);
			$(select).append(option);
		}
		$(select).val("");
		$(select).trigger("change");
	}
	
	function isValid() {
		var valid = true;
		
		if (global.required) {
			if (typeof global.value == "string")  {
				if (global.value.trim() == "") {
					valid = false;
					$(global.select).next("div.selectize-control").addClass("modal-field-invalid");
				}
			} else {
				if (!(global.value > 0)) {
					valid = false;
					$(global.select).next("div.selectize-control").addClass("modal-field-invalid");
				}
			}
		}
		return valid;
	}
	
	Selectize.prototype.selectizeField = function() {
		global.selectize = selectizeField(global.select);
	}
	
	Selectize.prototype.getSelectized = function() {
		return global.selectize;
	}
	
	Selectize.prototype.customPopulate = function(data) {
		customPopulate(data);
	}
	
	Selectize.prototype.isValid = function() {
		return isValid();
	}
	
	Selectize.prototype.required = function() {
		var span = document.createElement("span");
		$(span).append("*");
		$(global.label).append(span);
		
		global.required = true;
	}
	
	Selectize.prototype.clear = function() {
		$(global.select).val("");
		$(global.select).trigger("change");
	}
	
	Selectize.prototype.empty = function() {
		$(global.select).empty();
		
		var option = document.createElement("option");
		$(option).val("");
		$(option).text("");
		
		$(global.select).append(option);
		$(global.select).trigger("change");
	}
	
	Selectize.prototype.populate = function(data) {
		populate(data);
	}
	
	Selectize.prototype.remove = function() {
		$(global.wrapper).remove();
	}
	
	Selectize.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	Selectize.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	Selectize.prototype.triggerEvent = function() {
		$(global.select).trigger("change");
	}
	
	Selectize.prototype.enable = function() {
		$(global.select).attr("disabled", false);
	}
	
	Selectize.prototype.disable = function() {
		$(global.select).attr("disable", true);
	}
	
	Selectize.prototype.addStyle = function(style) {
		$(global.wrapper).addClass(style);
	}
	
	Selectize.prototype.removeStyle = function(style) {
		$(global.wrapper).removeClass(style);
	}
	
	Selectize.prototype.resetStyle = function() {
		$(global.wrapper).removeClass();
	}
	
	Selectize.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	Selectize.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	Selectize.prototype.setValue = function(value) {
		global.selectize.setValue(value, true);
	}
	
	Selectize.prototype.getValue = function() {
		var value = $(global.select).val()
		if (value) {
			value = value.join("|");
			return value;
		} else {
			return [];
		}
	}
	
	Selectize.prototype.setAttribute = function(key, value) {
		$(global.select).attr(key, value);
	}
	
	Selectize.prototype.getAttribute = function(key) {
		return $(global.select).attr(key);
	}
	
	Selectize.prototype.setData = function(key, data) {
		$(global.select).data(key, data);
	}
	
	Selectize.prototype.getData = function(key) {
		return $(global.select).data(key);
	}
	
	Selectize.prototype.getLabel = function() {
		return global.label;
	}
	
	Selectize.prototype.getSelect = function() {
		return global.select;
	}
	
	Selectize.prototype.getElement = function() {
		return global.wrapper;
	}
}

var DatePicker = function() {
	var global = {
		label: null,
		labelText: "",
		date: null,
		value: "",
		required: false,
		wrapper: null
	}
	
	initialize();
	
	function initialize() {
		var label = global.label = document.createElement("button");
		$(label).text(global.labelText);
		
		var date = global.date = document.createElement("input");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(label);
		$(wrapper).append(date);
		
		$(date).datepicker({
			showOn: "button",
			buttonImage: calendarIconUrl,
			buttonImageOnly: true,
			buttonText: "Select date",
			changeMonth: true,
			changeYear: true,
			yearRange: "1900:2030",
			dateFormat: 'mm/dd/yy',
			showButtonPanel: true
		});
		
		setDateHandler();
	}
	
	function setDateHandler() {
		$(global.date).on("change", function(e) {
			$(global.date).removeClass("modal-field-invalid");
			
			var value = $(this).val()
			if (value) {
				var split = value = value.split("/");
				global.value = split.join("");
			} else {
				global.value = "";
			}
		});
	}
	
	function isValid() {
		var valid = true;
		
		if (global.required) {
			if (!(global.value.trim())) {
				valid = false;
				$(global.date).addClass("modal-field-invalid");
			}
		}
		return valid;
	}
	
	DatePicker.prototype.isValid = function() {
		return isValid();
	}
	
	DatePicker.prototype.required = function() {
		var span = document.createElement("span");
		$(span).append("*");
		$(global.label).append(span);
		
		global.required = true;
	}
	
	DatePicker.prototype.clear = function() {
		global.value = "";
		$(global.date).val(global.value);
	}
	
	DatePicker.prototype.remove = function() {
		$(global.wrapper).remove();
	}
	
	DatePicker.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	DatePicker.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	DatePicker.prototype.enable = function() {
		$(global.date).attr("disabled", false);
	}
	
	DatePicker.prototype.disable = function() {
		$(global.date).attr("disable", true);
	}
	
	DatePicker.prototype.addStyle = function(style) {
		$(global.wrapper).addClass(style);
	}
	
	DatePicker.prototype.removeStyle = function(style) {
		$(global.wrapper).removeClass(style);
	}
	
	DatePicker.prototype.resetStyle = function() {
		$(global.wrapper).removeClass();
	}
	
	DatePicker.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	DatePicker.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	DatePicker.prototype.setValue = function(value) {
		global.value = value;
		$(global.date).val(global.value);
	}
	
	DatePicker.prototype.getValue = function() {
		return global.value;
	}
	
	DatePicker.prototype.setAttribute = function(key, value) {
		$(global.date).attr(key, value);
	}
	
	DatePicker.prototype.getAttribute = function(key) {
		return $(global.date).attr(key);
	}
	
	DatePicker.prototype.setData = function(key, data) {
		$(global.date).data(key, data);
	}
	
	DatePicker.prototype.getData = function(key) {
		return $(global.date).data(key);
	}
	
	DatePicker.prototype.getLabel = function() {
		return global.label;
	}
	
	DatePicker.prototype.getDatePicker = function() {
		return global.date;
	}
	
	DatePicker.prototype.getElement = function() {
		return global.wrapper;
	}
}

var CheckBox = function() {
	var global = {
		label: null,
		labelText: "",
		checkbox: null,
		checkIcon: null,
		value: "",
		status: false,
		required: false,
		wrapper: null
	}
	
	initialize();
	
	function initialize() {
		var checkbox = global.checkbox = document.createElement("input");
		$(checkbox).attr("type", checkbox);
		$(checkbox).val(global.value);
		
		var label = global.label = document.createElement("label");
		$(label).text(global.labelText);
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(checkbox);
		$(wrapper).append(label);
		
		setCheckboxHandler();
	}
	
	function setCheckboxHandler() {
		$(global.checkbox).on("change", function(e) {
			global.status = this.checked;
		});
	}
	
	function isValid() {
		var valid = true;
		
		if (global.required) {
			if (!(global.checkbox.checked)) {
				valid = false;
			}
		}
		return valid;
	}
	
	CheckBox.prototype.isValid = function() {
		return isValid();
	}
	
	CheckBox.prototype.required = function() {
		global.required = true;
	}
	
	CheckBox.prototype.remove = function() {
		$(global.wrapper).remove();
	}
	
	CheckBox.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	CheckBox.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	CheckBox.prototype.enable = function() {
		$(global.checkbox).attr("disabled", false);
	}
	
	CheckBox.prototype.disable = function() {
		$(global.select).attr("disable", true);
	}
	
	CheckBox.prototype.check = function() {
		if(!global.checkbox.checked) {
			$(global.checkbox).trigger("click");
		}
	}
	
	CheckBox.prototype.uncheck = function() {
		if(global.checkbox.checked) {
			$(global.checkbox).trigger("click");
		}
	}
	
	CheckBox.prototype.addStyle = function(style) {
		$(global.wrapper).addClass(style);
	}
	
	CheckBox.prototype.removeStyle = function(style) {
		$(global.wrapper).removeClass(style);
	}
	
	CheckBox.prototype.resetStyle = function() {
		$(global.wrapper).removeClass();
	}
	
	CheckBox.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	CheckBox.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	CheckBox.prototype.setValue = function(value) {
		global.value = value;
		$(global.checkbox).val(global.value);
	}
	
	CheckBox.prototype.getValue = function() {
		return global.value;
	}
	
	CheckBox.prototype.setStatus = function(status) {
		global.status = status;

		if (global.status != global.checkbox.checked) {
			$(global.checkbox).trigger("click");
		}
	}
	
	CheckBox.prototype.getStatus = function() {
		return global.status;
	}
	
	CheckBox.prototype.setAttribute = function(key, value) {
		$(global.checkbox).attr(key, value);
	}
	
	CheckBox.prototype.getAttribute = function(key) {
		return $(global.checkbox).attr(key);
	}
	
	CheckBox.prototype.setData = function(key, data) {
		$(global.checkbox).data(key, data);
	}
	
	CheckBox.prototype.getData = function(key) {
		return $(global.checkbox).data(key);
	}
	
	CheckBox.prototype.getLabel = function() {
		return global.label;
	}
	
	CheckBox.prototype.getCheckbox = function() {
		return global.checkbox;
	}
	
	CheckBox.prototype.getElement = function() {
		return global.wrapper;
	}
}

var TextArea = function() {
	var global = {
		label: null,
		labelText: "",
		textarea: null,
		value: "",
		required: false,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var label = global.label = document.createElement("button");
		$(label).text(global.labelText);
		
		var textarea = global.textarea = document.createElement("textarea");
		$(textarea).val(global.value);
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(label);
		$(wrapper).append(textarea);
		
		setTextAreaHandler();
	}
	
	function setTextAreaHandler() {
		$(global.textarea).on("blur", function(e) {
			global.value = $(this).val();
		}); 
	}
	
	function isValid() {
		var valid = true;
		
		if (global.required) {
			if (!(global.value.trim())) {
				valid = false;
			}
		}
		return valid;
	}
	
	TextArea.prototype.required = function() {
		var span = document.createElement("span");
		$(span).append("*");
		$(global.label).append(span);
		
		global.required = true;
	}
	
	TextArea.prototype.clear = function() {
		global.value = "";
		$(global.textarea).val(global.value);
	}
	
	TextArea.prototype.remove = function() {
		$(global.wrapper).remove();
	}
	
	TextArea.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	TextArea.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	TextArea.prototype.enable = function() {
		$(global.textarea).attr("disabled", false);
	}
	
	TextArea.prototype.disable = function() {
		$(global.textarea).attr("disable", true);
	}
	
	TextArea.prototype.addStyle = function(style) {
		$(global.wrapper).addClass(style);
	}
	
	TextArea.prototype.removeStyle = function(style) {
		$(global.wrapper).removeClass(style);
	}
	
	TextArea.prototype.resetStyle = function() {
		$(global.wrapper).removeClass();
	}
	
	TextArea.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	TextArea.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	TextArea.prototype.setValue = function(value) {
		global.value = value;
		$(global.textarea).val(global.value);
	}
	
	TextArea.prototype.getValue = function() {
		return global.value;
	}
	
	TextArea.prototype.setAttribute = function(key, value) {
		$(global.textarea).attr(key, value);
	}
	
	TextArea.prototype.getAttribute = function(key) {
		return $(global.textarea).attr(key);
	}
	
	TextArea.prototype.setData = function(key, data) {
		$(global.textarea).data(key, data);
	}
	
	TextArea.prototype.getData = function(key) {
		return $(global.textarea).data(key);
	}
	
	TextArea.prototype.getLabel = function() {
		return global.label;
	}
	
	TextArea.prototype.getTextArea = function() {
		return global.textarea;
	}
	
	TextArea.prototype.getElement = function() {
		return global.wrapper;
	}
}

var Button = function() {
	var global = {
		labelText: "",
		button: null
	};
	
	initialize();
	
	function initialize() {
		var button = global.button = document.createElement("button");
		$(button).text(global.labelText);
	}
	
	Button.prototype.remove = function() {
		$(global.button).remove();
	}
	
	Button.prototype.show = function() {
		$(global.button).show();
	}
	
	Button.prototype.hide = function() {
		$(global.button).hide();
	}
	
	Button.prototype.triggerEvent = function() {
		$(global.Button).trigger("change");
	}
	
	Button.prototype.enable = function() {
		$(global.button).attr("disabled", false);
	}
	
	Button.prototype.disable = function() {
		$(global.button).attr("disable", true);
	}
	
	Button.prototype.addStyle = function(style) {
		$(global.button).addClass(style);
	}
	
	Button.prototype.removeStyle = function(style) {
		$(global.button).removeClass(style);
	}
	
	Button.prototype.resetStyle = function() {
		$(global.button).removeClass();
	}
	
	Button.prototype.setButtonText = function(text) {
		global.labelText = text;
		$(global.button).text(global.labelText);
	}
	
	Button.prototype.getButtonText = function() {
		return global.labelText;
	}
	
	Button.prototype.setAttribute = function(key, value) {
		$(global.button).attr(key, value);
	}
	
	Button.prototype.getAttribute = function(key) {
		return $(global.button).attr(key);
	}
	
	Button.prototype.setData = function(key, data) {
		$(global.button).data(key, data);
	}
	
	Button.prototype.getData = function(key) {
		return $(global.button).data(key);
	}
	
	Button.prototype.getButton = function() {
		return global.button;
	}
}



var Container = function() {
	var global = {
		container: null
	};
	
	initialize();
	
	function initialize() {
		var container = global.container = document.createElement("div");
	}
	
	Container.prototype.resetScroll = function() {
		$(global.container).animate({scrollTop: 0}, "fast");
	}
	
	Container.prototype.empty = function() {
		$(global.container).empty();
	}

	Container.prototype.remove = function() {
		$(global.container).remove();
	}
	
	Container.prototype.show = function() {
		$(global.container).show();
	}
	
	Container.prototype.hide = function() {
		$(global.container).hide();
	}
	
	Container.prototype.addStyle = function(style) {
		$(global.container).addClass(style);
	}
	
	Container.prototype.removeStyle = function(style) {
		$(global.container).removeClass(style);
	}
	
	Container.prototype.resetStyle = function() {
		$(global.container).removeClass();
	}
	
	Container.prototype.getContainer = function() {
		return global.container;
	}
}

var Label = function() {
	var global = {
		labelText: "",
		label: null
	};
	
	initialize();
	
	function initialize() {
		var label = global.label = document.createElement("button");
		$(label).text(global.labelText);
	}
	
	Label.prototype.remove = function() {
		$(global.label).remove();
	}
	
	Label.prototype.show = function() {
		$(global.label).show();
	}
	
	Label.prototype.hide = function() {
		$(global.label).hide();
	}
	
	Label.prototype.addStyle = function(style) {
		$(global.label).addClass(style);
	}
	
	Label.prototype.removeStyle = function(style) {
		$(global.label).removeClass(style);
	}
	
	Label.prototype.resetStyle = function(){
		$(global.label).removeClass();
	}
	
	Label.prototype.setLabelText = function(text) {
		global.labelText = text;
		$(global.label).text(global.labelText);
	}
	
	Label.prototype.getLabelText = function() {
		return global.labelText;
	}
	
	Label.prototype.getLabel = function() {
		return global.label;
	}
}

var Paragraph = function() {
	var global = {
		paragraphText: "",
		paragraph: null
	}
	
	initialize();
	
	function initialize() {
		var paragraph = global.paragraph = document.createElement("p");
		$(paragraph).append(global.paragraphText);
	}
	
	Paragraph.prototype.remove = function() {
		$(global.paragraph).remove();
	}
	
	Paragraph.prototype.show = function() {
		$(global.paragraph).show();
	}
	
	Paragraph.prototype.hide = function() {
		$(global.paragraph).hide();
	}
	
	Paragraph.prototype.addStyle = function(style) {
		$(global.paragraph).addClass(style);
	}
	
	Paragraph.prototype.removeStyle = function(style) {
		$(global.paragraph).removeClass(style);
	}
	
	Paragraph.prototype.resetStyle = function() {
		$(global.paragraph).removeClass();
	}
	
	Paragraph.prototype.setParagraphText = function(text) {
		global.paragraphText = text;
		$(global.paragraph).html(global.paragraphText);
	}
	
	Paragraph.prototype.getParagraphText = function() {
		return global.paragraphText;
	}
	
	Paragraph.prototype.getParagraph = function() {
		return global.paragraph;
	}
}

var Span = function() {
	var global = {
		spanText: "",
		span: null
	}
	
	initialize();
	
	function initialize() {
		var span = global.span = document.createElement("span");
		$(span).append(global.spanText);
	}
	
	Span.prototype.remove = function() {
		$(global.span).remove();
	}
	
	Span.prototype.show = function() {
		$(global.span).show();
	}
	
	Span.prototype.hide = function() {
		$(global.span).hide();
	}
	
	Span.prototype.addStyle = function(style) {
		$(global.span).addClass(style);
	}
	
	Span.prototype.removeStyle = function(style) {
		$(global.span).removeClass(style);
	}
	
	Span.prototype.resetStyle = function() {
		$(global.span).removeClass();
	}
	
	Span.prototype.setSpanText = function(text) {
		global.spanText = text;
		$(global.span).html(global.spanText);
	}
	
	Span.prototype.getSpanText = function() {
		return global.spanText;
	}
	
	Span.prototype.getSpan = function() {
		return global.span;
	}
}

var Utilities = function() {
	function getFirstLetter (txt) {
		var letter = txt.charAt(0);
		return letter;
	}

	function getRandomColor() {
		var hex = Math.floor(Math.random() * 0xFFFFFF);
		return "#" + ("000000" + hex.toString(16)).substr(-6);
	}
	
	function convertToTitleCase(camelCase) {
		return camelCase.replace(/([A-Z])/g, function(match) {
			return " " + match;
		}).replace(/^./, function(match) {
			return match.toUpperCase();
		});
	}
	
	function currencyFormat(symbol, amount) {
		var number = Number(amount);
	    return symbol + number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	
	function numberFormat(value) {
		var number = Number(value);
	    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function getYearArray(minYear) {
		var yearArr = [];
	    var date = new Date();
	    var maxYear = date.getFullYear();
	    
	    for (var i = maxYear; i >= minYear; i--) {
	    	yearArr.push(i);
	    }
	    
	    return yearArr;
	}
	
	function isEmpty(value) {
		var empty = false;
		if (value === undefined || value === null || value.trim() === "") {
			empty = true;
		}
		return empty;
	}
	
	Utilities.prototype.isEmpty = function(value) {
		return isEmpty(value);
	}
	
	Utilities.prototype.getFirstLetter = function(txt) {
		return getFirstLetter(txt);
	}
	
	Utilities.prototype.getRandomColor = function() {
		return getRandomColor();
	}
	
	Utilities.prototype.convertToTitleCase = function(camelCase) {
		return convertToTitleCase(camelCase);
	}
	
	Utilities.prototype.currencyFormat = function(symbol, amount) {
		return currencyFormat(symbol, amount);
	}
	
	Utilities.prototype.numberFormat = function(value) {
		return numberFormat(value);
	}
	
	Utilities.prototype.getYearArray = function(minYear) {
		return getYearArray(minYear);
	}
}