var UtilitiesMenu = function() {
	var global = {
		utilTabs: null,
		wrapper: null	
	};
	
	initialize();
	
	function initialize() {
		var header = $.extend(true, {}, new Header());
		$(header.getWidget()).addClass("utilities-header");
		
		var utilHeader = document.createElement("div");
		$(utilHeader).addClass("utilities-header-pnl");
		$(utilHeader).append(header.getWidget());
		
		var utilTabs = global.utilTabs = $.extend(true, {}, new Tabs());
		$(utilTabs.getWidget()).addClass("utilities-tabs");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("utilities-menu");
		$(wrapper).append(utilHeader);
		$(wrapper).append(utilTabs.getWidget());
	}
	
	UtilitiesMenu.prototype.show = function() {
		$(global.wrapper).show();
	}
	
	UtilitiesMenu.prototype.hide = function() {
		$(global.wrapper).hide();
	}
	
	UtilitiesMenu.prototype.getUtilTabs = function() {
		return global.utilTabs;
	}
	
	UtilitiesMenu.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var Header = function() {
	var global = {
		titleText: "Utilities",
		descriptionText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
		
		title: null,
		description: null,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var title = global.title = document.createElement("button");
		$(title).text(global.titleText);
		
		var description = global.description = document.createElement("button");
		$(description).text(global.descriptionText);
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).append(title);
		$(wrapper).append(description);
	}
	
	Header.prototype.setTitle = function(title) {
		global.titleText = title;
		$(global.title).text(global.titleText);
	}
	
	Header.prototype.setDescription = function(description) {
		global.descriptionText = description;
		$(global.description).text(global.descriptionText);
	}
	
	Header.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var Tabs = function() {
	var global = {
		options: [],
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var wrapper = global.wrapper = document.createElement("div");
		setOptions();
	}
	
	function setOptions() {
		$(global.wrapper).empty();
		global.options = [];
		
		for (var key in utilTabConfig) {
			var optAttributes = utilTabConfig[key];
			var id = optAttributes.id;
			var title = optAttributes.title;
			var description = optAttributes.description;
			var parameters = optAttributes.parameters;
			
			var option = $.extend(true, {}, new Option());
			option.setTitle(title);
			option.setDescription(description);
			option.setId(id);
			option.setParameters(parameters);
			
			$(global.wrapper).append(option.getWidget());
			global.options.push(option);
		}
	}
	
	Tabs.prototype.getOptions = function() {
		return global.options;
	}
	
	Tabs.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var Option = function() {
	var global = {
		titleText: "",
		descriptionText: "",
		id: "",
		parameters: [],
		
		title: null,
		description: null,
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var title = global.title = document.createElement("button");
		var description = global.description = document.createElement("button");
		var icon = document.createElement("div");
		
		var wrapper = global.wrapper = document.createElement("div");
		$(wrapper).addClass("utilities-tab-option");
		
		$(wrapper).append(title);
		$(wrapper).append(description);
		$(wrapper).append(icon)
	}
	
	Option.prototype.getId = function() {
		return global.id;
	}
	
	Option.prototype.setId = function(id) {
		global.id = id;
	}
	
	Option.prototype.getParameters = function() {
		return global.parameters;
	}
	
	Option.prototype.setParameters = function(parameters) {
		global.parameters = parameters;
	}
	
	Option.prototype.getTitle = function(parameters) {
		return global.titleText;
	}
	
	Option.prototype.setTitle = function(title) {
		global.titleText = title;
		$(global.title).text(global.titleText);
	}
	
	Option.prototype.getDescription = function(parameters) {
		return global.descriptionText;
	}
	
	Option.prototype.setDescription = function(description) {
		global.descriptionText = description;
		$(global.description).text(global.descriptionText);
	}
	
	Option.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var OptionModal = function() {
	var global = {
		header: null,
		fields: null,
		cancel: null,
		run: null,
		
		overlay: null
	};
	
	initialize();
	
	function initialize() {
		var header = global.header = $.extend(true, {}, new Header());
		$(header.getWidget()).addClass("option-popup-header");
		
		var fields = global.fields = $.extend(true, {}, new ModalFields());
		$(fields.getWidget()).addClass("option-popup-fields");
		
		var controls = buildControls();
		
		var wrapper = document.createElement("div");
		$(wrapper).addClass("option-popup-basepnl");
		$(wrapper).append(header.getWidget());
		$(wrapper).append(fields.getWidget());
		$(wrapper).append(controls);
		
		var overlay = global.overlay = document.createElement("div");
		$(overlay).addClass("option-popup-overlay");
		$(overlay).append(wrapper);
		
		setHandlers();
	}
	
	function buildControls() {
		var cancel = global.cancel = $.extend(true, {}, new Button());
		cancel.addStyle("option-popup-cancel");
		cancel.setButtonText("CANCEL");
		
		var run = global.run = $.extend(true, {}, new Button());
		run.addStyle("option-popup-run");
		run.setButtonText("RUN");
		
		var controls = document.createElement("div");
		$(controls).addClass("option-popup-controls");
		$(controls).append(cancel.getButton());
		$(controls).append(run.getButton());
		
		return controls;
	}
	
	function setHandlers() {
		var cancelBtn = global.cancel.getButton();
		
		$(cancelBtn).on("click tap", function() {
			$(global.overlay).hide();
		});
	}
	
	function getModalData() {
		var fieldsData = [];
		var validCtr = 0;
		var orderedFields = global.fields.getOrderedFields();
		
		for (var i = 0; i < orderedFields.length; i++) {
			var field = orderedFields[i];
			var isValid = field.isValid();
			if (isValid) {
				var value = field.getValue();
				fieldsData.push(value);
			} else {
				validCtr++;
			}
		}
		
		if (validCtr > 0) {
			return "Please fill up required fields.";
		}
		
		return fieldsData;
	}
	
	OptionModal.prototype.getModalData = function() {
		return getModalData();
	}
	
	OptionModal.prototype.show = function() {
		$(global.overlay).show();
	}
	
	OptionModal.prototype.hide = function() {
		$(global.overlay).hide();
	}
	
	OptionModal.prototype.getCancel = function(title) {
		return global.cancel
	}
	
	OptionModal.prototype.getRun = function(title) {
		return global.run
	}
	
	OptionModal.prototype.setModalTitle = function(title) {
		global.header.setTitle(title);
	}
	
	OptionModal.prototype.setModalDescription = function(description) {
		global.header.setDescription(description);
	}
	
	OptionModal.prototype.setModalFields = function(parameters) {
		global.fields.setModalFields(parameters);
	}
	
	OptionModal.prototype.getModalFields = function() {
		return global.fields.getModalFields();
	}
	
	OptionModal.prototype.getModalFieldByKey = function(key) {
		return global.fields.getModalFieldByKey(key);
	}

	OptionModal.prototype.getWidget = function() {
		return global.overlay;
	}
}

var ModalFields = function() {
	var global = {
		modalFields: {},
		orderedFields: [],
		
		wrapper: null
	};
	
	initialize();
	
	function initialize() {
		var wrapper = global.wrapper = document.createElement("div");
	}
	
	function setModalFields(parameters) {
		$(global.wrapper).empty();
		global.modalFields = {};
		global.orderedFields = [];
		
		for (var i = 0; i < parameters.length; i++) {
			var fieldAttributes = parameters[i];
			var key = fieldAttributes.key;
			var label = fieldAttributes.label;
			var type = fieldAttributes.type;
			var required = fieldAttributes.required;
			var lookup = fieldAttributes.lookup;
			var fieldBuilder = $.extend(true, {}, new FieldBuilder(key, label, type, required, lookup));
			var field = fieldBuilder.getField().getElement();
			$(field).addClass("option-popup-field");
			
			$(global.wrapper).append(field);
			global.modalFields[key] = fieldBuilder.getField();
			global.orderedFields.push(fieldBuilder.getField());
		}
	}
	
	ModalFields.prototype.getModalFieldByKey = function(key) {
		return global.modalFields[key];
	}
	
	ModalFields.prototype.getModalFields = function() {
		return global.modalFields;
	}
	
	ModalFields.prototype.getOrderedFields = function() {
		return global.orderedFields;
	}
	
	ModalFields.prototype.setModalFields = function(parameters) {
		setModalFields(parameters);
	}
	
	ModalFields.prototype.getWidget = function() {
		return global.wrapper;
	}
}

var FieldBuilder = function(key, label, type, required, lookup) {
	var global = {
		field: null
	};
	
	initialize();
	
	function initialize() {
		var field;
		
		if (type === "date") {
			field = $.extend(true, {}, new DatePicker());
		} else if (type === "multipleSelect") {
			field = $.extend(true, {}, new Selectize());
			field.addStyle("field-multipleselect");
		} else if (type === "list") {
			field = $.extend(true, {}, new Select());
		}

		field.setLabelText(label);
		field.setAttribute("holder", key);
		field.setAttribute("fieldType", type);
		
		if (required) {
			field.required();
		}
		
		if (lookup) {
			field.setAttribute("lookup", lookup);
		}

		global.field = field;
	}
	
	FieldBuilder.prototype.getField = function() {
		return global.field;
	}
}