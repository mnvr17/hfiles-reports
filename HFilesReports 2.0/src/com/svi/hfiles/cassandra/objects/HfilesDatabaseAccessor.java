package com.svi.hfiles.cassandra.objects;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;

@Accessor
public interface HfilesDatabaseAccessor {
	@Query("SELECT * FROM person WHERE company_id = :company_id ")
	Result<Person> getAllPersonsFromcompany(@Param("company_id") String paramString);

	@Query("SELECT * FROM person")
	Result<Person> getAllPerson();
	
	@Query("SELECT * FROM relationship WHERE company_id = :company_id ")
	Result<Relationship> getAllRelationshipsFromcompany(@Param("company_id") String paramString);
	
	@Query("SELECT * FROM relationship")
	Result<Relationship> getAllRelationship();
	  
}
