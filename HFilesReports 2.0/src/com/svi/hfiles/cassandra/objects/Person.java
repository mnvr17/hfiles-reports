package com.svi.hfiles.cassandra.objects;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Table(keyspace = "hfiles", name = "person", caseSensitiveKeyspace = false, caseSensitiveTable = false)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Person {
  @PartitionKey(0)
  @Column(name = "company_id")
  private String companyId;
  
  @PartitionKey(1)
  @Column(name = "person_id")
  private String personId;
  
  @Column(name = "employee_id")
  private String employeeId;
  
  @Column(name = "person_type")
  private String personType;
  
  @Column(name = "first_name")
  private String firstName;
  
  @Column(name = "middle_name")
  private String middleName;
  
  @Column(name = "last_name")
  private String lastName;
  
  @Column(name = "suffix")
  private String suffix;
  
  @Column(name = "maiden_name")
  private String maidenName;
  
  @Column(name = "nickname")
  private List<String> nickname;
  
  @Column(name = "job_title")
  private String jobTitle;
  
  @Column(name = "position_type")
  private String positionType;
  
  @Column(name = "position_title")
  private List<String> positionTitle;
  
  @Column(name = "hire_date")
  private Date hireDate;
  
  @Column(name = "regularization_date")
  private String regularizationDate;
  
  @Column(name = "end_contract_date")
  private Date endContractDate;
  
  @Column(name = "separation_date")
  private String separationDate;
  
  @Column(name = "reason_rl")
  private String reasonRl;
  
  @Column(name = "date_last_promotion")
  private Date dateLastPromotion;
  
  @Column(name = "employment_status")
  private String employmentStatus;
  
  @Column(name = "employment_type")
  private String employmentType;
  
  @Column(name = "org_chart_hierarchy")
  private Map<String, String> orgChartHierarchy;
  
  @Column(name = "previous_employee_no")
  private String previousEmployeeNo;
  
  @Column(name = "biometric_id")
  private String biometricId;
  
  @Column(name = "height")
  private BigDecimal height;
  
  @Column(name = "weight")
  private BigDecimal weight;
  
  @Column(name = "gender")
  private String gender;
  
  @Column(name = "civil_status")
  private String civilStatus;
  
  @Column(name = "dependency_status")
  private String dependencyStatus;
  
  @Column(name = "birthdate")
  private Date birthdate;
  
  @Column(name = "birthplace")
  private String birthplace;
  
  @Column(name = "citizenship")
  private List<String> citizenship;
  
  @Column(name = "nationality")
  private String nationality;
  
  @Column(name = "religion")
  private String religion;
  
  @Column(name = "salary_type")
  private String salaryType;
  
  @Column(name = "salary_level")
  private String salaryLevel;
  
  @Column(name = "basic_monthly_rate")
  private String basicMonthlyRate;
  
  @Column(name = "basic_daily_rate")
  private String basicDailyRate;
  
  @Column(name = "basic_hourly_rate")
  private String basicHourlyRate;
  
  @Column(name = "annualized_tax")
  private BigDecimal annualizedTax;
  
  @Column(name = "cumulative_salary")
  private BigDecimal cumulativeSalary;
  
  @Column(name = "bank_account")
  private String bankAccount;
  
  @Column(name = "deduction_exemption_code")
  private List<String> deductionExemptionCode;
  
  @Column(name = "hdmf_contribution")
  private BigDecimal hdmfContribution;
  
  @Column(name = "philhealth_contribution")
  private BigDecimal philhealthContribution;
  
  @Column(name = "minimum_wage_indicator")
  private boolean minimumWageIndicator;
  
  @Column(name = "tax_code")
  private String taxCode;
  
  @Column(name = "with_dependents")
  private boolean withDependents;
  
  @Column(name = "present_address_line1")
  private String presentAddressLine1;
  
  @Column(name = "present_address_line2")
  private String presentAddressLine2;
  
  @Column(name = "present_barangay")
  private String presentBarangay;
  
  @Column(name = "present_city")
  private String presentCity;
  
  @Column(name = "present_province")
  private String presentProvince;
  
  @Column(name = "present_country")
  private String presentCountry;
  
  @Column(name = "present_zip_code")
  private String presentZipCode;
  
  @Column(name = "permanent_address_line1")
  private String permanentAddressLine1;
  
  @Column(name = "permanent_address_line2")
  private String permanentAddressLine2;
  
  @Column(name = "permanent_barangay")
  private String permanentBarangay;
  
  @Column(name = "permanent_city")
  private String permanentCity;
  
  @Column(name = "permanent_province")
  private String permanentProvince;
  
  @Column(name = "permanent_country")
  private String permanentCountry;
  
  @Column(name = "permanent_zip_code")
  private String permanentZipCode;
  
  @Column(name = "tin_no")
  private String tinNo;
  
  @Column(name = "sss_no")
  private String sssNo;
  
  @Column(name = "gsis_no")
  private String gsisNo;
  
  @Column(name = "philhealth_no")
  private String philhealthNo;
  
  @Column(name = "hdmf_no")
  private String hdmfNo;
  
  @Column(name = "voter_id_no")
  private String voterIdNo;
  
  @Column(name = "passport_no")
  private String passportNo;
  
  @Column(name = "driver_license_no")
  private String driverLicenseNo;
  
  @Column(name = "postal_id_no")
  private String postalIdNo;
  
  @Column(name = "national_id")
  private String nationalId;
  
  @Column(name = "senior_citizen_id")
  private String seniorCitizenId;
  
  @Column(name = "nbi_clearance")
  private String nbiClearance;
  
  @Column(name = "police_clearance")
  private String policeClearance;
  
  @Column(name = "umid")
  private String umid;
  
  @Column(name = "seaman_book")
  private List<String> seamanBook;
  
  @Column(name = "company_reach")
  private String companyReach;
  
  @Column(name = "previous_company_name")
  private String previousCompanyName;
  
  @Column(name = "previous_company_address")
  private String previousCompanyAddress;
  
  @Column(name = "previous_company_rdo_code")
  private String previousCompanyRdoCode;
  
  @Column(name = "previous_company_taxable_income")
  private BigDecimal previousCompanyTaxableIncome;
  
  @Column(name = "previous_company_salary")
  private BigDecimal previousCompanySalary;
  
  @Column(name = "vl_balance")
  private BigDecimal vlBalance;
  
  @Column(name = "sl_balance")
  private BigDecimal slBalance;
  
  @Column(name = "company_email")
  private List<String> companyEmail;
  
  @Column(name = "mobile")
  private List<String> mobile;
  
  @Column(name = "personal_email")
  private List<String> personalEmail;
  
  @Column(name = "telephone")
  private List<String> telephone;
  
  @Column(name = "interests")
  private List<String> interests;
  
  @Column(name = "date_created")
  private Date dateCreated;
  
  @Column(name = "date_last_updated")
  private Date dateLastUpdated;
  
  @Column(name = "place_marriage")
  private String placeMarriage;
  
  @Column(name = "date_marriage")
  private Date dateMarriage;
  
  @Column(name = "occupation")
  private String occupation;
  
  @Column(name = "place_of_employment")
  private String placeOfEmployment;
  
  @Column(name = "career_path")
  private String careerPath;
  
  @Column(name = "fte")
  private String fte;
  
  @Column(name = "hobbies")
  private List<String> hobbies;
  
  @Column(name = "build")
  private String build;
  
  @Column(name = "complexion")
  private String complexion;
  
  @Column(name = "eyes")
  private String eyes;
  
  @Column(name = "hair")
  private String hair;
  
  @Column(name = "marks")
  private String marks;
  
  @Column(name = "other_features")
  private String otherFeatures;
  
  @Column(name = "defects")
  private String defects;
  
  @Column(name = "recent_serious_illness")
  private String recentSeriousIllness;
  
  @Column(name = "salary_computation_basis")
  private String salaryComputationBasis;
  
  @Column(name = "rate")
  private BigDecimal rate;
  
  @Column(name = "bank_name")
  private String bankName;
  
  @Column(name = "account_name")
  private String accountName;
  
  @Column(name = "purpose")
  private String purpose;
  
  @Column(name = "other_bank_account")
  private List<String> otherBankAccount;
  
  @Column(name = "previous_company_tax_withheld")
  private BigDecimal previousCompanyTaxWithheld;
  
  public String getPersonId() {
    return this.personId;
  }
  
  public void setPersonId(String personId) {
    this.personId = personId;
  }
  
  public String getEmployeeId() {
    return this.employeeId;
  }
  
  public void setEmployeeId(String employeeId) {
    this.employeeId = employeeId;
  }
  
  public String getCompanyId() {
    return this.companyId;
  }
  
  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }
  
  public String getPersonType() {
    return this.personType;
  }
  
  public void setPersonType(String personType) {
    this.personType = personType;
  }
  
  public String getFirstName() {
    return this.firstName;
  }
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  public String getMiddleName() {
    return this.middleName;
  }
  
  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }
  
  public String getLastName() {
    return this.lastName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getSuffix() {
    return this.suffix;
  }
  
  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }
  
  public String getMaidenName() {
    return this.maidenName;
  }
  
  public void setMaidenName(String maidenName) {
    this.maidenName = maidenName;
  }
  
  public List<String> getNickname() {
    return this.nickname;
  }
  
  public void setNickname(List<String> nickName) {
    this.nickname = nickName;
  }
  
  public String getJobTitle() {
    return this.jobTitle;
  }
  
  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }
  
  public String getPositionType() {
    return this.positionType;
  }
  
  public void setPositionType(String positionType) {
    this.positionType = positionType;
  }
  
  public List<String> getPositionTitle() {
    return this.positionTitle;
  }
  
  public void setPositionTitle(List<String> positionTitle) {
    this.positionTitle = positionTitle;
  }
  
  public Date getHireDate() {
    return this.hireDate;
  }
  
  public void setHireDate(Date hireDate) {
    this.hireDate = hireDate;
  }
  
  public String getRegularizationDate() {
    return this.regularizationDate;
  }
  
  public void setRegularizationDate(String regularizationDate) {
    this.regularizationDate = regularizationDate;
  }
  
  public Date getEndContractDate() {
    return this.endContractDate;
  }
  
  public void setEndContractDate(Date endContractDate) {
    this.endContractDate = endContractDate;
  }
  
  public String getSeparationDate() {
    return this.separationDate;
  }
  
  public void setSeparationDate(String separationDate) {
    this.separationDate = separationDate;
  }
  
  public String getReasonRl() {
    return this.reasonRl;
  }
  
  public void setReasonRl(String reasonRl) {
    this.reasonRl = reasonRl;
  }
  
  public Date getDateLastPromotion() {
    return this.dateLastPromotion;
  }
  
  public void setDateLastPromotion(Date dateLastPromotion) {
    this.dateLastPromotion = dateLastPromotion;
  }
  
  public String getEmploymentStatus() {
    return this.employmentStatus;
  }
  
  public void setEmploymentStatus(String employmentStatus) {
    this.employmentStatus = employmentStatus;
  }
  
  public String getEmploymentType() {
    return this.employmentType;
  }
  
  public void setEmploymentType(String employmentType) {
    this.employmentType = employmentType;
  }
  
  public Map<String, String> getOrgChartHierarchy() {
    return this.orgChartHierarchy;
  }
  
  public void setOrgChartHierarchy(Map<String, String> orgChartHierarchy) {
    this.orgChartHierarchy = orgChartHierarchy;
  }
  
  public String getPreviousEmployeeNo() {
    return this.previousEmployeeNo;
  }
  
  public void setPreviousEmployeeNo(String previousEmployeeNo) {
    this.previousEmployeeNo = previousEmployeeNo;
  }
  
  public String getBiometricId() {
    return this.biometricId;
  }
  
  public void setBiometricId(String biometricId) {
    this.biometricId = biometricId;
  }
  
  public BigDecimal getHeight() {
    return this.height;
  }
  
  public void setHeight(BigDecimal height) {
    this.height = height;
  }
  
  public BigDecimal getWeight() {
    return this.weight;
  }
  
  public void setWeight(BigDecimal weight) {
    this.weight = weight;
  }
  
  public String getGender() {
    return this.gender;
  }
  
  public void setGender(String gender) {
    this.gender = gender;
  }
  
  public String getCivilStatus() {
    return this.civilStatus;
  }
  
  public void setCivilStatus(String civilStatus) {
    this.civilStatus = civilStatus;
  }
  
  public String getDependencyStatus() {
    return this.dependencyStatus;
  }
  
  public void setDependencyStatus(String dependencyStatus) {
    this.dependencyStatus = dependencyStatus;
  }
  
  public Date getBirthdate() {
    return this.birthdate;
  }
  
  public void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }
  
  public String getBirthplace() {
    return this.birthplace;
  }
  
  public void setBirthplace(String birthplace) {
    this.birthplace = birthplace;
  }
  
  public List<String> getCitizenship() {
    return this.citizenship;
  }
  
  public void setCitizenship(List<String> citizenship) {
    this.citizenship = citizenship;
  }
  
  public String getNationality() {
    return this.nationality;
  }
  
  public void setNationality(String nationality) {
    this.nationality = nationality;
  }
  
  public String getReligion() {
    return this.religion;
  }
  
  public void setReligion(String religion) {
    this.religion = religion;
  }
  
  public String getSalaryType() {
    return this.salaryType;
  }
  
  public void setSalaryType(String salaryType) {
    this.salaryType = salaryType;
  }
  
  public String getBasicMonthlyRate() {
    return this.basicMonthlyRate;
  }
  
  public void setBasicMonthlyRate(String basicMonthlyRate) {
    this.basicMonthlyRate = basicMonthlyRate;
  }
  
  public String getBasicDailyRate() {
    return this.basicDailyRate;
  }
  
  public void setBasicDailyRate(String basicDailyRate) {
    this.basicDailyRate = basicDailyRate;
  }
  
  public String getBasicHourlyRate() {
    return this.basicHourlyRate;
  }
  
  public void setBasicHourlyRate(String basicHourlyRate) {
    this.basicHourlyRate = basicHourlyRate;
  }
  
  public BigDecimal getAnnualizedTax() {
    return this.annualizedTax;
  }
  
  public void setAnnualizedTax(BigDecimal annualizedTax) {
    this.annualizedTax = annualizedTax;
  }
  
  public BigDecimal getCumulativeSalary() {
    return this.cumulativeSalary;
  }
  
  public void setCumulativeSalary(BigDecimal cumulativeSalary) {
    this.cumulativeSalary = cumulativeSalary;
  }
  
  public String getBankAccount() {
    return this.bankAccount;
  }
  
  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }
  
  public List<String> getOtherBankAccount() {
    return this.otherBankAccount;
  }
  
  public void setOtherBankAccount(List<String> otherBankAccount) {
    this.otherBankAccount = otherBankAccount;
  }
  
  public List<String> getDeductionExemptionCode() {
    return this.deductionExemptionCode;
  }
  
  public void setDeductionExemptionCode(List<String> deductionExemptionCode) {
    this.deductionExemptionCode = deductionExemptionCode;
  }
  
  public BigDecimal getHdmfContribution() {
    return this.hdmfContribution;
  }
  
  public void setHdmfContribution(BigDecimal hdmfContribution) {
    this.hdmfContribution = hdmfContribution;
  }
  
  public BigDecimal getPhilhealthContribution() {
    return this.philhealthContribution;
  }
  
  public void setPhilhealthContribution(BigDecimal philhealthContribution) {
    this.philhealthContribution = philhealthContribution;
  }
  
  public boolean isMinimumWageIndicator() {
    return this.minimumWageIndicator;
  }
  
  public void setMinimumWageIndicator(boolean minimumWageIndicator) {
    this.minimumWageIndicator = minimumWageIndicator;
  }
  
  public String getTaxCode() {
    return this.taxCode;
  }
  
  public void setTaxCode(String taxCode) {
    this.taxCode = taxCode;
  }
  
  public boolean isWithDependents() {
    return this.withDependents;
  }
  
  public void setWithDependents(boolean withDependents) {
    this.withDependents = withDependents;
  }
  
  public String getPresentAddressLine1() {
    return this.presentAddressLine1;
  }
  
  public void setPresentAddressLine1(String presentAddressLine1) {
    this.presentAddressLine1 = presentAddressLine1;
  }
  
  public String getPresentAddressLine2() {
    return this.presentAddressLine2;
  }
  
  public void setPresentAddressLine2(String presentAddressLine2) {
    this.presentAddressLine2 = presentAddressLine2;
  }
  
  public String getPresentBarangay() {
    return this.presentBarangay;
  }
  
  public void setPresentBarangay(String presentBarangay) {
    this.presentBarangay = presentBarangay;
  }
  
  public String getPresentCity() {
    return this.presentCity;
  }
  
  public void setPresentCity(String presentCity) {
    this.presentCity = presentCity;
  }
  
  public String getPresentProvince() {
    return this.presentProvince;
  }
  
  public void setPresentProvince(String presentProvince) {
    this.presentProvince = presentProvince;
  }
  
  public String getPresentCountry() {
    return this.presentCountry;
  }
  
  public void setPresentCountry(String presentCountry) {
    this.presentCountry = presentCountry;
  }
  
  public String getPresentZipCode() {
    return this.presentZipCode;
  }
  
  public void setPresentZipCode(String presentZipCode) {
    this.presentZipCode = presentZipCode;
  }
  
  public String getPermanentAddressLine1() {
    return this.permanentAddressLine1;
  }
  
  public void setPermanentAddressLine1(String permanentAddressLine1) {
    this.permanentAddressLine1 = permanentAddressLine1;
  }
  
  public String getPermanentAddressLine2() {
    return this.permanentAddressLine2;
  }
  
  public void setPermanentAddressLine2(String permanentAddressLine2) {
    this.permanentAddressLine2 = permanentAddressLine2;
  }
  
  public String getPermanentBarangay() {
    return this.permanentBarangay;
  }
  
  public void setPermanentBarangay(String permanentBarangay) {
    this.permanentBarangay = permanentBarangay;
  }
  
  public String getPermanentCity() {
    return this.permanentCity;
  }
  
  public void setPermanentCity(String permanentCity) {
    this.permanentCity = permanentCity;
  }
  
  public String getPermanentProvince() {
    return this.permanentProvince;
  }
  
  public void setPermanentProvince(String permanentProvince) {
    this.permanentProvince = permanentProvince;
  }
  
  public String getPermanentCountry() {
    return this.permanentCountry;
  }
  
  public void setPermanentCountry(String permanentCountry) {
    this.permanentCountry = permanentCountry;
  }
  
  public String getPermanentZipCode() {
    return this.permanentZipCode;
  }
  
  public void setPermanentZipCode(String permanentZipCode) {
    this.permanentZipCode = permanentZipCode;
  }
  
  public String getTinNo() {
    return this.tinNo;
  }
  
  public void setTinNo(String tinNo) {
    this.tinNo = tinNo;
  }
  
  public String getSssNo() {
    return this.sssNo;
  }
  
  public void setSssNo(String sssNo) {
    this.sssNo = sssNo;
  }
  
  public String getGsisNo() {
    return this.gsisNo;
  }
  
  public void setGsisNo(String gsisNo) {
    this.gsisNo = gsisNo;
  }
  
  public String getPhilhealthNo() {
    return this.philhealthNo;
  }
  
  public void setPhilhealthNo(String philhealthNo) {
    this.philhealthNo = philhealthNo;
  }
  
  public String getHdmfNo() {
    return this.hdmfNo;
  }
  
  public void setHdmfNo(String hdmfNo) {
    this.hdmfNo = hdmfNo;
  }
  
  public String getVoterIdNo() {
    return this.voterIdNo;
  }
  
  public void setVoterIdNo(String voterIdNo) {
    this.voterIdNo = voterIdNo;
  }
  
  public String getPassportNo() {
    return this.passportNo;
  }
  
  public void setPassportNo(String passportNo) {
    this.passportNo = passportNo;
  }
  
  public String getDriverLicenseNo() {
    return this.driverLicenseNo;
  }
  
  public void setDriverLicenseNo(String driverLicenseNo) {
    this.driverLicenseNo = driverLicenseNo;
  }
  
  public String getPostalIdNo() {
    return this.postalIdNo;
  }
  
  public void setPostalIdNo(String postalIdNo) {
    this.postalIdNo = postalIdNo;
  }
  
  public String getNationalId() {
    return this.nationalId;
  }
  
  public void setNationalId(String nationalId) {
    this.nationalId = nationalId;
  }
  
  public String getSeniorCitizenId() {
    return this.seniorCitizenId;
  }
  
  public void setSeniorCitizenId(String seniorCitizenId) {
    this.seniorCitizenId = seniorCitizenId;
  }
  
  public String getNbiClearance() {
    return this.nbiClearance;
  }
  
  public void setNbiClearance(String nbiClearance) {
    this.nbiClearance = nbiClearance;
  }
  
  public String getPoliceClearance() {
    return this.policeClearance;
  }
  
  public void setPoliceClearance(String policeClearance) {
    this.policeClearance = policeClearance;
  }
  
  public String getUmid() {
    return this.umid;
  }
  
  public void setUmid(String umid) {
    this.umid = umid;
  }
  
  public List<String> getSeamanBook() {
    return this.seamanBook;
  }
  
  public void setSeamanBook(List<String> seamanBook) {
    this.seamanBook = seamanBook;
  }
  
  public String getCompanyReach() {
    return this.companyReach;
  }
  
  public void setCompanyReach(String companyReach) {
    this.companyReach = companyReach;
  }
  
  public String getPreviousCompanyName() {
    return this.previousCompanyName;
  }
  
  public void setPreviousCompanyName(String previousCompanyName) {
    this.previousCompanyName = previousCompanyName;
  }
  
  public String getPreviousCompanyAddress() {
    return this.previousCompanyAddress;
  }
  
  public void setPreviousCompanyAddress(String previousCompanyAddress) {
    this.previousCompanyAddress = previousCompanyAddress;
  }
  
  public String getPreviousCompanyRdoCode() {
    return this.previousCompanyRdoCode;
  }
  
  public void setPreviousCompanyRdoCode(String previousCompanyRdoCode) {
    this.previousCompanyRdoCode = previousCompanyRdoCode;
  }
  
  public BigDecimal getPreviousCompanyTaxableIncome() {
    return this.previousCompanyTaxableIncome;
  }
  
  public void setPreviousCompanyTaxableIncome(BigDecimal previousCompanyTaxableIncome) {
    this.previousCompanyTaxableIncome = previousCompanyTaxableIncome;
  }
  
  public BigDecimal getPreviousCompanySalary() {
    return this.previousCompanySalary;
  }
  
  public void setPreviousCompanySalary(BigDecimal previousCompanySalary) {
    this.previousCompanySalary = previousCompanySalary;
  }
  
  public BigDecimal getVlBalance() {
    return this.vlBalance;
  }
  
  public void setVlBalance(BigDecimal vlBalance) {
    this.vlBalance = vlBalance;
  }
  
  public BigDecimal getSlBalance() {
    return this.slBalance;
  }
  
  public void setSlBalance(BigDecimal slBalance) {
    this.slBalance = slBalance;
  }
  
  public List<String> getCompanyEmail() {
    return this.companyEmail;
  }
  
  public void setCompanyEmail(List<String> companyEmail) {
    this.companyEmail = companyEmail;
  }
  
  public List<String> getMobile() {
    return this.mobile;
  }
  
  public void setMobile(List<String> mobile) {
    this.mobile = mobile;
  }
  
  public List<String> getPersonalEmail() {
    return this.personalEmail;
  }
  
  public void setPersonalEmail(List<String> personalEmail) {
    this.personalEmail = personalEmail;
  }
  
  public List<String> getTelephone() {
    return this.telephone;
  }
  
  public void setTelephone(List<String> telephone) {
    this.telephone = telephone;
  }
  
  public List<String> getInterests() {
    return this.interests;
  }
  
  public void setInterests(List<String> interests) {
    this.interests = interests;
  }
  
  public Date getDateCreated() {
    return this.dateCreated;
  }
  
  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }
  
  public Date getDateLastUpdated() {
    return this.dateLastUpdated;
  }
  
  public void setDateLastUpdated(Date dateLastUpdated) {
    this.dateLastUpdated = dateLastUpdated;
  }
  
  public String getPlaceMarriage() {
    return this.placeMarriage;
  }
  
  public void setPlaceMarriage(String placeMarriage) {
    this.placeMarriage = placeMarriage;
  }
  
  public Date getDateMarriage() {
    return this.dateMarriage;
  }
  
  public void setDateMarriage(Date dateMarriage) {
    this.dateMarriage = dateMarriage;
  }
  
  public String getOccupation() {
    return this.occupation;
  }
  
  public void setOccupation(String occupation) {
    this.occupation = occupation;
  }
  
  public String getPlaceOfEmployment() {
    return this.placeOfEmployment;
  }
  
  public void setPlaceOfEmployment(String placeOfEmployment) {
    this.placeOfEmployment = placeOfEmployment;
  }
  
  public String getCareerPath() {
    return this.careerPath;
  }
  
  public void setCareerPath(String careerPath) {
    this.careerPath = careerPath;
  }
  
  public String getFte() {
    return this.fte;
  }
  
  public void setFte(String fte) {
    this.fte = fte;
  }
  
  public List<String> getHobbies() {
    return this.hobbies;
  }
  
  public void setHobbies(List<String> hobbies) {
    this.hobbies = hobbies;
  }
  
  public String getBuild() {
    return this.build;
  }
  
  public void setBuild(String build) {
    this.build = build;
  }
  
  public String getComplexion() {
    return this.complexion;
  }
  
  public void setComplexion(String complexion) {
    this.complexion = complexion;
  }
  
  public String getEyes() {
    return this.eyes;
  }
  
  public void setEyes(String eyes) {
    this.eyes = eyes;
  }
  
  public String getHair() {
    return this.hair;
  }
  
  public void setHair(String hair) {
    this.hair = hair;
  }
  
  public String getMarks() {
    return this.marks;
  }
  
  public void setMarks(String marks) {
    this.marks = marks;
  }
  
  public String getOtherFeatures() {
    return this.otherFeatures;
  }
  
  public void setOtherFeatures(String otherFeatures) {
    this.otherFeatures = otherFeatures;
  }
  
  public String getDefects() {
    return this.defects;
  }
  
  public void setDefects(String defects) {
    this.defects = defects;
  }
  
  public String getRecentSeriousIllness() {
    return this.recentSeriousIllness;
  }
  
  public void setRecentSeriousIllness(String recentSeriousIllness) {
    this.recentSeriousIllness = recentSeriousIllness;
  }
  
  public String getSalaryComputationBasis() {
    return this.salaryComputationBasis;
  }
  
  public void setSalaryComputationBasis(String salaryComputationBasis) {
    this.salaryComputationBasis = salaryComputationBasis;
  }
  
  public BigDecimal getRate() {
    return this.rate;
  }
  
  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }
  
  public String getBankName() {
    return this.bankName;
  }
  
  public void setBankName(String bankName) {
    this.bankName = bankName;
  }
  
  public String getAccountName() {
    return this.accountName;
  }
  
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }
  
  public String getPurpose() {
    return this.purpose;
  }
  
  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }
  
  public String getSalaryLevel() {
    return this.salaryLevel;
  }
  
  public void setSalaryLevel(String salaryLevel) {
    this.salaryLevel = salaryLevel;
  }
  
  public BigDecimal getPreviousCompanyTaxWithheld() {
    return this.previousCompanyTaxWithheld;
  }
  
  public void setPreviousCompanyTaxWithheld(BigDecimal previousCompanyTaxWithheld) {
    this.previousCompanyTaxWithheld = previousCompanyTaxWithheld;
  }
  
  public String toString() {
    ObjectMapper a = new ObjectMapper();
    String writeValueAsString = "";
    try {
      writeValueAsString = a.writeValueAsString(this);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    return writeValueAsString;
  }
}

