package com.svi.hfiles.cassandra.objects;

import java.util.List;

import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name = "relationship",
caseSensitiveKeyspace = false, 
caseSensitiveTable = false)

@JsonIgnoreProperties(ignoreUnknown = true)
public class Relationship {

	@PartitionKey(0)
	@Column(name = "company_id")
	private String companyId;
	
	@ClusteringColumn(0)
	@Column(name = "person_id")
	private String personId;
	
	@ClusteringColumn(1)
	@Column(name = "other_person_id")
	private String otherPersonId;
	
	@Column(name = "relationship")
	private List<String> relationship;
	
	@Column(name = "company_relative_indicator")
	private boolean companyRelativeIndicator;
	
	@Column(name = "emergency_contact_indicator")
	private boolean emegencyContactIndicator;

	
	@Column(name = "family_beneficiary_indicator")
	private boolean familyBeneficiaryIndicator;
	
	@Column(name = "living_abroad_indicator")
	private boolean livingAbroadIndicator;
	

	public boolean isFamilyBeneficiaryIndicator() {
		return familyBeneficiaryIndicator;
	}

	public void setFamilyBeneficiaryIndicator(boolean familyBeneficiaryIndicator) {
		this.familyBeneficiaryIndicator = familyBeneficiaryIndicator;
	}

	public boolean isCompanyRelativeIndicator() {
		return companyRelativeIndicator;
	}

	public void setCompanyRelativeIndicator(boolean companyRelativeIndicator) {
		this.companyRelativeIndicator = companyRelativeIndicator;
	}

	public boolean isEmegencyContactIndicator() {
		return emegencyContactIndicator;
	}

	public void setEmegencyContactIndicator(boolean emegencyContactIndicator) {
		this.emegencyContactIndicator = emegencyContactIndicator;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getOtherPersonId() {
		return otherPersonId;
	}

	public void setOtherPersonId(String otherPersonId) {
		this.otherPersonId = otherPersonId;
	}

	public List<String> getRelationship() {
		return relationship;
	}

	public void setRelationship(List<String> relationship) {
		this.relationship = relationship;
	}

	
	public boolean isLivingAbroadIndicator() {
		return livingAbroadIndicator;
	}

	public void setLivingAbroadIndicator(boolean livingAbroadIndicator) {
		this.livingAbroadIndicator = livingAbroadIndicator;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("relationship{companyId=");
		builder.append(companyId);
		builder.append("|personId=");
		builder.append(personId);
		builder.append("|otherPersonId=");
		builder.append(otherPersonId);
		if (relationship != null) {
			for (String rel : relationship) {
				builder.append("|relationship=");
				builder.append(rel);
			}
		}
		builder.append("|companyRelativeIndicator=");
		builder.append(companyRelativeIndicator);
		builder.append("|emegencyContactIndicator=");
		builder.append(emegencyContactIndicator);
		builder.append("|familyBeneficiaryIndicator=");
		builder.append(familyBeneficiaryIndicator);
		builder.append("}");
		return builder.toString();
	}

}
