package com.svi.hfiles.globals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum AppConfig {
	GFS_LOCAL_ENV("GFS_LOCAL_ENV"),
	GFS_FOLDER("GFS_FOLDER"),
	GFS_LOCAL("GFS_LOCAL"),
	IS_DEV("IS_DEV"),
	BUCKET_NAME("BUCKET_NAME"),
	ROOT_FOLDER("ROOT_FOLDER"),
	REPORTS_FOLDER_NAME("REPORTS_FOLDER_NAME"),
	GFS_ANNUAL_REPORTS_FOLDER("GFS_ANNUAL_REPORTS_FOLDER"),
	GFS_MONTHLY_REPORTS_FOLDER("GFS_MONTHLY_REPORTS_FOLDER"),
	GFS_WEEKLY_REPORTS_FOLDER("GFS_WEEKLY_REPORTS_FOLDER"),
	GFS_DAILY_REPORTS_FOLDER("GFS_DAILY_REPORTS_FOLDER"),
	DAILY_REPORTS_FREQUENCY_KEY("DAILY_REPORTS_FREQUENCY_KEY"),
	WEEKLY_REPORTS_FREQUENCY_KEY("WEEKLY_REPORTS_FREQUENCY_KEY"),
	MONTHLY_REPORTS_FREQUENCY_KEY("MONTHLY_REPORTS_FREQUENCY_KEY"),
	ANNUAL_REPORTS_FREQUENCY_KEY("ANNUAL_REPORTS_FREQUENCY_KEY"),
	RUNNABLE_JARS_LOGS_PATH("RUNNABLE_JARS_LOGS_PATH"),
	TNA_XML_DIR("TNA_XML_DIR"),
	TK_PHOTO_DIR("TK_PHOTO_DIR"),
	TNA_PARSER_FOLDER("TNA_PARSER_FOLDER"),
	NOT_FOUND_IMG("NOT_FOUND_IMG"),
	ERROR_IMG("ERROR_IMG"),
	EMPLOYEE_DIRECTORY("EMPLOYEE_DIRECTORY"),
	EMPLOYEE_BIRTHDAY("EMPLOYEE_BIRTHDAY"),
	EMPLOYEE_SEPERATION("EMPLOYEE_SEPERATION"),
	EMERGENCY_CONTACTS("EMERGENCY_CONTACTS"),
	NEW_HIRE("NEW_HIRE"),
	CASSANDRA_IP_ADD("CASSANDRA_SERVER"),
	CASSANDRA_KEYSPACE("CASSANDRA_KEYSPACE")
	;

	private String value = "";
	private static Properties prop;

	private AppConfig(String value) {
		this.value = value;
	}

	public String value() {
		return prop.getProperty(value).trim();
	}

	public static void setContext(InputStream inputStream) {
		synchronized (inputStream) {
			if (prop == null) {
				try {
					prop = new Properties();
					prop.load(inputStream);
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					try {
						inputStream.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}
}
