package com.svi.hfiles.globals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class RunnableJarsConfig {
	private static Properties prop;

	public static String getRunnableJarPath(String reportType) {
		return prop.getProperty(reportType);
	}

	public static void setContext(InputStream inputStream) {
		synchronized (inputStream) {
			if (prop == null) {
				try {
					prop = new Properties();
					prop.load(inputStream);
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					try {
						inputStream.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}
}
