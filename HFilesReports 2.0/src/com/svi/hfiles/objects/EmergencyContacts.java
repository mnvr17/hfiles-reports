package com.svi.hfiles.objects;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmergencyContacts {
	private String employeeId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String employmentType;
	private String relationshipFirstName;
	private String relationshipMiddleName;
	private String relationshipLastName;
	private String relationship;
	private List<String> mobile;
	private List<String> telephone;
	private String permanentAddressLine1;
	private String permanentAddressLine2;
	private String permanentBarangay;
	private String permanentCity;
	private String permanentProvince;
	private String permanentCountry;
	private String permanentZipCode;
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmploymentType() {
		return employmentType;
	}
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}
	public String getRelationshipFirstName() {
		return relationshipFirstName;
	}
	public void setRelationshipFirstName(String relationshipFirstName) {
		this.relationshipFirstName = relationshipFirstName;
	}
	public String getRelationshipMiddleName() {
		return relationshipMiddleName;
	}
	public void setRelationshipMiddleName(String relationshipMiddleName) {
		this.relationshipMiddleName = relationshipMiddleName;
	}
	public String getRelationshipLastName() {
		return relationshipLastName;
	}
	public void setRelationshipLastName(String relationshipLastName) {
		this.relationshipLastName = relationshipLastName;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public List<String> getMobile() {
		return mobile;
	}
	public void setMobile(List<String> mobile) {
		this.mobile = mobile;
	}
	public List<String> getTelephone() {
		return telephone;
	}
	public void setTelephone(List<String> telephone) {
		this.telephone = telephone;
	}
	public String getPermanentAddressLine1() {
		return permanentAddressLine1;
	}
	public void setPermanentAddressLine1(String permanentAddressLine1) {
		this.permanentAddressLine1 = permanentAddressLine1;
	}
	public String getPermanentAddressLine2() {
		return permanentAddressLine2;
	}
	public void setPermanentAddressLine2(String permanentAddressLine2) {
		this.permanentAddressLine2 = permanentAddressLine2;
	}
	public String getPermanentBarangay() {
		return permanentBarangay;
	}
	public void setPermanentBarangay(String permanentBarangay) {
		this.permanentBarangay = permanentBarangay;
	}
	public String getPermanentCity() {
		return permanentCity;
	}
	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}
	public String getPermanentProvince() {
		return permanentProvince;
	}
	public void setPermanentProvince(String permanentProvince) {
		this.permanentProvince = permanentProvince;
	}
	public String getPermanentCountry() {
		return permanentCountry;
	}
	public void setPermanentCountry(String permanentCountry) {
		this.permanentCountry = permanentCountry;
	}
	public String getPermanentZipCode() {
		return permanentZipCode;
	}
	public void setPermanentZipCode(String permanentZipCode) {
		this.permanentZipCode = permanentZipCode;
	}
	
	
	
}
