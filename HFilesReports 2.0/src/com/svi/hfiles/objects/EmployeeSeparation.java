package com.svi.hfiles.objects;

import java.util.Date;

public class EmployeeSeparation {
	private String employeeId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String employmentType;
	private String employmentStatus;
	private Date hireDate;
	private String separationDate;
	private Date endContractDate;
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmploymentType() {
		return employmentType;
	}
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public Date getHireDate() {
		return hireDate;
	}
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	public String getSeparationDate() {
		return separationDate;
	}
	public void setSeparationDate(String separationDate) {
		this.separationDate = separationDate;
	}
	public Date getEndContractDate() {
		return endContractDate;
	}
	public void setEndContractDate(Date endContractDate) {
		this.endContractDate = endContractDate;
	}
}
