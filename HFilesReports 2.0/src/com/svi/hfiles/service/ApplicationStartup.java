package com.svi.hfiles.service;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.svi.hfiles.globals.AppConfig;
import com.svi.hfiles.globals.AwsCredentialsConfig;
import com.svi.hfiles.globals.RunnableJarsConfig;

@WebListener
public class ApplicationStartup implements ServletContextListener {

	private static final String CONFIG_INI_LOCATION = "CONFIG_INI_LOCATION";
	private static final String AWS_CREDENTIALS_INI_LOCATION = "AWS_CREDENTIALS_INI_LOCATION";
	private static final String RUNNABLE_JARS_CONFIG_LOCATION = "RUNNABLE_JARS_CONFIG_LOCATION";
	
	private ServletContext sc;

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		//
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		this.sc = event.getServletContext();
		setConfig();
		System.setProperty("rootPath", this.sc.getRealPath("/"));
		System.setProperty("webservice.rootPathForLogger", this.sc.getRealPath("/"));
	}

	private void setConfig() {
		AppConfig.setContext(sc.getResourceAsStream(sc.getInitParameter(CONFIG_INI_LOCATION)));
		String awsCredentialsConfigPath = sc.getInitParameter(AWS_CREDENTIALS_INI_LOCATION);
		AwsCredentialsConfig.setContext(awsCredentialsConfigPath);
		RunnableJarsConfig.setContext(sc.getResourceAsStream(sc.getInitParameter(RUNNABLE_JARS_CONFIG_LOCATION)));
	}

}
