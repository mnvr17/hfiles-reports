package com.svi.hfiles.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.datastax.driver.mapping.Result;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sun.jersey.core.header.ContentDisposition;
import com.svi.gfs.main.Gfs;
import com.svi.gfs.object.FileObject;
import com.svi.hfiles.cassandra.objects.Person;
import com.svi.hfiles.globals.AppConfig;
import com.svi.hfiles.globals.AwsCredentialsConfig;
import com.svi.hfiles.globals.RunnableJarsConfig;
import com.svi.hfiles.objects.EmergencyContacts;
import com.svi.hfiles.objects.EmployeeBirthday;
import com.svi.hfiles.objects.EmployeeDirectory;
import com.svi.hfiles.objects.EmployeeSeparation;
import com.svi.hfiles.objects.NewHire;
import com.svi.hfiles.utils.CassandraConnectionUtil;
import com.svi.hfiles.utils.XmlParser;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

@Path("/documents")
public class HfilesReportSvc {
	
	public static String[] months = {"January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December"};

	@GET
	@Path("/{companyName}/{reportName}")
	public Response downloadReports(@PathParam("companyName") String companyName, 
			@PathParam("reportName") String reportName) {
		
		companyName = "CDCI";
		reportName = "Employee_Birthday";
		
		String jrxmlPath = getJrxmlPath(StringUtils.trimToEmpty(reportName));
		System.out.println("JRXML PATH: "+jrxmlPath);
		File theFile = new File(jrxmlPath);
		JasperDesign jasperDesign;
		try {
			jasperDesign = JRXmlLoader.load(theFile);
			Result<Person> persons = CassandraConnectionUtil.getUserAccessor().getAllPersonsFromcompany(companyName);
			List<Person> personList = persons.all();
			JRBeanCollectionDataSource jrBeanCDS = generateJRBeanCDS(reportName, personList);
			String string = "emergencyContacts";
			Map<String, Object> parameters = new HashMap<>();
			parameters.put(string, jrBeanCDS);
			
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
			
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMddyyyy");
			LocalDateTime now = LocalDateTime.now();
//			File file = new File(reportName + "_" + dtf.format(now) + ".pdf");
			File file = new File("EXAMPLE.pdf");
			OutputStream os = new FileOutputStream(file);

			JasperExportManager.exportReportToPdfFile(jasperPrint, os.toString());
			
			ResponseBuilder response = Response.ok((Object) file);
			response.header("Content-Disposition", "attachment; filename=" + file.getName());
			System.out.println("SUCCESS GENERATING PDF");
			os.close();
			return response.build();
		} catch (JRException e) {
			// TODO Auto-generated catch block
			System.out.println("JRException Error: " + e.getMessage());
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("FileNotFoundError Error: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IOException Error: " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private JRBeanCollectionDataSource generateJRBeanCDS(String reportName, List<Person> personList) {
		// TODO Auto-generated method stub
		if(reportName.trim().toLowerCase().contains("Employee_Birthday".toLowerCase())){
			List<EmployeeBirthday> employeeBirthday = generateEBList(personList);
			return new JRBeanCollectionDataSource(employeeBirthday);
		} else if(reportName.trim().toLowerCase().contains("Emergency_Contacts".toLowerCase())){
			List<EmergencyContacts> emergencyContactsList = generateECList(personList);
			return new JRBeanCollectionDataSource(emergencyContactsList);
		} else if(reportName.trim().toLowerCase().contains("Employee_Directory".toLowerCase())){
			List<EmployeeDirectory> emergencyDirectoryList = generateEDList(personList);
			return new JRBeanCollectionDataSource(emergencyDirectoryList);
		} else if(reportName.trim().toLowerCase().contains("Employee_Seperation".toLowerCase())){
			List<EmployeeSeparation> emergencySeparationList = generateESList(personList);
			return new JRBeanCollectionDataSource(emergencySeparationList);
		} else if(reportName.trim().toLowerCase().contains("New_Hire".toLowerCase())){
			List<NewHire> newHireList = generateNHList(personList);
			return new JRBeanCollectionDataSource(newHireList);
		}
		
		return new JRBeanCollectionDataSource(personList);
	}
	
	private List<NewHire> generateNHList(List<Person> personList) {
		List<NewHire> newHireList = new ArrayList<NewHire>();
		for (Person person : personList) {
			NewHire nh = new NewHire();
			nh.setEmployeeId(person.getEmployeeId());
			nh.setFirstName(person.getFirstName());
			nh.setMiddleName(person.getMiddleName());
			nh.setLastName(person.getLastName());
			nh.setEmploymentType(person.getEmploymentType());
			nh.setHireDate(person.getHireDate());
			nh.setJobTitle(person.getJobTitle());
			nh.setPositionTitle(person.getPositionTitle());
			nh.setSalaryLevel(person.getSalaryLevel());
			nh.setSalaryType(person.getSalaryType());
			nh.setDepartment(person.getOrgChartHierarchy().get("Department"));
			nh.setDivision(person.getOrgChartHierarchy().get("Division"));
			nh.setGroup(person.getOrgChartHierarchy().get("Group"));
			nh.setSection(person.getOrgChartHierarchy().get("Section"));
			newHireList.add(nh);
		}
		return newHireList;
	}

	private List<EmployeeSeparation> generateESList(List<Person> personList) {
		List<EmployeeSeparation> emergencySeparationList = new ArrayList<EmployeeSeparation>();
		for (Person person : personList) {
			EmployeeSeparation es = new EmployeeSeparation();
			es.setEmployeeId(person.getEmployeeId());
			es.setFirstName(person.getFirstName());
			es.setMiddleName(person.getMiddleName());
			es.setLastName(person.getLastName());
			es.setEmploymentType(person.getEmploymentType());
			es.setHireDate(person.getHireDate());
			es.setSeparationDate(person.getSeparationDate());
			es.setEndContractDate(person.getEndContractDate());
			emergencySeparationList.add(es);
		}
		return emergencySeparationList;
	}

	private List<EmployeeDirectory> generateEDList(List<Person> personList) {
		List<EmployeeDirectory> employeeDirectoryList = new ArrayList<EmployeeDirectory>();
		for (Person person : personList) {
			EmployeeDirectory ed = new EmployeeDirectory();
			ed.setEmployeeId(person.getEmployeeId());
			ed.setFirstName(person.getFirstName());
			ed.setMiddleName(person.getMiddleName());
			ed.setLastName(person.getLastName());
			ed.setEmploymentType(person.getEmploymentType());
			ed.setBirthdate(person.getBirthdate());
			ed.setMobile(person.getMobile());
			ed.setPermanentAddressLine1(person.getPermanentAddressLine1());
			ed.setPermanentAddressLine2(person.getPermanentAddressLine2());
			ed.setPermanentBarangay(person.getPermanentBarangay());
			ed.setPermanentCity(person.getPermanentCity());
			ed.setPermanentProvince(person.getPermanentProvince());
			ed.setPermanentCountry(person.getPermanentCountry());
			ed.setPermanentZipCode(person.getPermanentZipCode());
			employeeDirectoryList.add(ed);
		}
		return employeeDirectoryList;
	}

	private List<EmployeeBirthday> generateEBList(List<Person> personList) {
		List<EmployeeBirthday> employeeBirthdayList = new ArrayList<EmployeeBirthday>();
		for (Person person : personList) {
			EmployeeBirthday eb = new EmployeeBirthday();
			eb.setEmployeeId(person.getEmployeeId());
			eb.setFirstName(person.getFirstName());
			eb.setMiddleName(person.getMiddleName());
			eb.setLastName(person.getLastName());
			eb.setEmploymentType(person.getEmploymentType());
			eb.setBirthdate(person.getBirthdate());
			employeeBirthdayList.add(eb);
		}
		return employeeBirthdayList;
	}

	private List<EmergencyContacts> generateECList(List<Person> personList) {
		List<EmergencyContacts> emergencyContactsList = new ArrayList<EmergencyContacts>();
		for (Person person : personList) {
			EmergencyContacts ec = new EmergencyContacts();
			ec.setEmployeeId(person.getEmployeeId());
			ec.setFirstName(person.getFirstName());
			ec.setMiddleName(person.getMiddleName());
			ec.setLastName(person.getLastName());
			ec.setEmploymentType(person.getEmploymentType());
			ec.setMobile(person.getMobile());
			ec.setTelephone(person.getTelephone());
			ec.setPermanentAddressLine1(person.getPermanentAddressLine1());
			ec.setPermanentAddressLine2(person.getPermanentAddressLine2());
			ec.setPermanentBarangay(person.getPermanentBarangay());
			ec.setPermanentCity(person.getPermanentCity());
			ec.setPermanentProvince(person.getPermanentProvince());
			ec.setPermanentCountry(person.getPermanentCountry());
			ec.setPermanentZipCode(person.getPermanentZipCode());
			emergencyContactsList.add(ec);
		}
		return emergencyContactsList;
	}

	private String getJrxmlPath(String reportName) {
		String jrxmlPath = "";
//		String str = AppConfig.EMERGENCY_CONTACTS.value();
		if(StringUtils.trimToEmpty(AppConfig.EMPLOYEE_BIRTHDAY.toString()).toLowerCase()
				.contains(reportName.trim().toLowerCase())){
			jrxmlPath = AppConfig.EMPLOYEE_BIRTHDAY.value();
		} else if((AppConfig.EMERGENCY_CONTACTS.toString()).toLowerCase()
				.contains(reportName.trim().toLowerCase())){
			jrxmlPath = AppConfig.EMERGENCY_CONTACTS.value();
		} else if(StringUtils.trimToEmpty(AppConfig.EMPLOYEE_DIRECTORY.toString()).toLowerCase()
				.contains(reportName.trim().toLowerCase())){
			jrxmlPath = AppConfig.EMPLOYEE_DIRECTORY.value();
		} else if(StringUtils.trimToEmpty(AppConfig.EMPLOYEE_SEPERATION.toString()).toLowerCase()
				.contains(reportName.trim().toLowerCase())){
			jrxmlPath = AppConfig.EMPLOYEE_SEPERATION.value();
		}  else if(StringUtils.trimToEmpty(AppConfig.NEW_HIRE.toString()).toLowerCase()
				.contains(reportName.trim().toLowerCase())){
			jrxmlPath = AppConfig.NEW_HIRE.value();
		}
		return jrxmlPath;
	}

	@GET
	@Path("/report/download/{companyId}/{frequency}/{gfsKey}/{fileId}")
	public Response downloadDocuments(@PathParam("companyId") String companyId,
			@PathParam("frequency") String frequency, @PathParam("gfsKey") String gfsKey,
			@PathParam("fileId") String fileId) throws IOException {
		InputStream in = null;
		Gfs gfs = null;
		String gfsEnv = "";

		boolean isGfsLocal = AppConfig.GFS_LOCAL.value().trim().equalsIgnoreCase("Y");
		boolean isDev = AppConfig.IS_DEV.value().trim().equalsIgnoreCase("Y");

		if (isGfsLocal) {
			gfsEnv = AppConfig.GFS_LOCAL_ENV.value() + File.separator + AppConfig.GFS_FOLDER.value() + File.separator
					+ companyId + File.separator + AppConfig.REPORTS_FOLDER_NAME.value() + File.separator;
		} else {
			gfsEnv = AppConfig.ROOT_FOLDER.value() + File.separator + AppConfig.GFS_FOLDER.value() + File.separator
					+ companyId + File.separator + AppConfig.REPORTS_FOLDER_NAME.value() + File.separator;
		}

		if (frequency.equalsIgnoreCase(AppConfig.DAILY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_DAILY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.WEEKLY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_WEEKLY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.MONTHLY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_MONTHLY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.ANNUAL_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_ANNUAL_REPORTS_FOLDER.value();
		}

		if (isGfsLocal) {
			gfs = Gfs.newLocalStorageBuilder(gfsEnv).build();
		} else {
			if (isDev) {
				gfs = Gfs.newS3StorageBuilder(AppConfig.BUCKET_NAME.value(), gfsEnv)
						.withCredentials(AwsCredentialsConfig.ACCESS_KEY_ID.value(),
								AwsCredentialsConfig.SECRET_KEY_ID.value())
						.withProxy(AwsCredentialsConfig.IP.value(), AwsCredentialsConfig.PORT.value()).build();
			} else {
				gfs = Gfs.newS3StorageBuilder(AppConfig.BUCKET_NAME.value(), gfsEnv).build();
			}
		}

		List<FileObject> files = gfs.retrieve(gfsKey);

		boolean found = false;
		String mt = "application/octet-stream";
		ContentDisposition contentDisposition = null;

		for (FileObject file : files) {
			if (file.getFileId().equals(fileId)) {
				in = new ByteArrayInputStream(file.getFileBlob());
				contentDisposition = ContentDisposition.type("inline").fileName(file.getFileName())
						.creationDate(new Date()).build();
				found = true;
				break;
			}
		}

		gfs.close();

		if (found) {
			return Response.ok(in, mt).header("Content-Disposition", contentDisposition).build();
		} else {
			// 280 - custom code for gfs file not found
			return Response.status(280).build();
		}

	}

	@GET
	@Path("/report/view/{companyId}/{frequency}/{gfsKey}/{fileId}")
	public Response viewReport(@PathParam("companyId") String companyId, @PathParam("frequency") String frequency,
			@PathParam("gfsKey") String gfsKey, @PathParam("fileId") String fileId) throws IOException {
		InputStream in = null;
		Gfs gfs = null;
		String gfsEnv = "";

		boolean isGfsLocal = AppConfig.GFS_LOCAL.value().trim().equalsIgnoreCase("Y");
		boolean isDev = AppConfig.IS_DEV.value().trim().equalsIgnoreCase("Y");

		if (isGfsLocal) {
			gfsEnv = AppConfig.GFS_LOCAL_ENV.value() + File.separator + AppConfig.GFS_FOLDER.value() + File.separator
					+ companyId + File.separator + AppConfig.REPORTS_FOLDER_NAME.value() + File.separator;
		} else {
			gfsEnv = AppConfig.ROOT_FOLDER.value() + File.separator + AppConfig.GFS_FOLDER.value() + File.separator
					+ companyId + File.separator + AppConfig.REPORTS_FOLDER_NAME.value() + File.separator;
		}

		if (frequency.equalsIgnoreCase(AppConfig.DAILY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_DAILY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.WEEKLY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_WEEKLY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.MONTHLY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_MONTHLY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.ANNUAL_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_ANNUAL_REPORTS_FOLDER.value();
		}

		if (isGfsLocal) {
			gfs = Gfs.newLocalStorageBuilder(gfsEnv).build();
		} else {
			if (isDev) {
				gfs = Gfs.newS3StorageBuilder(AppConfig.BUCKET_NAME.value(), gfsEnv)
						.withCredentials(AwsCredentialsConfig.ACCESS_KEY_ID.value(),
								AwsCredentialsConfig.SECRET_KEY_ID.value())
						.withProxy(AwsCredentialsConfig.IP.value(), AwsCredentialsConfig.PORT.value()).build();
			} else {
				gfs = Gfs.newS3StorageBuilder(AppConfig.BUCKET_NAME.value(), gfsEnv).build();
			}
		}

		boolean found = false;

		List<FileObject> files = gfs.retrieve(gfsKey);
		String mt = "application/xml";
		ContentDisposition contentDisposition = null;
		for (FileObject file : files) {
			if (file.getFileId().equals(fileId)) {
				in = new ByteArrayInputStream(file.getFileBlob());
				contentDisposition = ContentDisposition.type("inline").fileName(file.getFileName())
						.creationDate(new Date()).build();

				String upperCaseFileName = file.getFileName().toUpperCase();
				if (upperCaseFileName.contains(".PDF")) {
					mt = "application/pdf";
				} else if (upperCaseFileName.contains(".JSON")) {
					mt = "application/json";
				} else {
					mt = URLConnection.guessContentTypeFromName(file.getFileName());
				}
				if (null == mt) {
					mt = "application/octet-stream";
				}

				found = true;
				break;
			}
		}

		gfs.close();
		if (found) {
			return Response.ok(in, mt).header("Content-Disposition", contentDisposition).build();
		} else {
			// 280 - custom code for gfs file not found
			return Response.status(280).build();
		}
	}

	@GET
	@Path("/report/list/{companyId}/{frequency}/{reportType}")
	public Response getReportsList(@PathParam("companyId") String companyId, @PathParam("frequency") String frequency,
			@PathParam("reportType") String reportType) throws IOException {
		String gfsEnv = "";

		boolean isGfsLocal = AppConfig.GFS_LOCAL.value().trim().equalsIgnoreCase("Y");
		boolean isDev = AppConfig.IS_DEV.value().trim().equalsIgnoreCase("Y");
		boolean needsSortingByMonthName = false;

		if (isGfsLocal) {
			gfsEnv = AppConfig.GFS_LOCAL_ENV.value() + File.separator + AppConfig.GFS_FOLDER.value() + File.separator
					+ companyId + File.separator + AppConfig.REPORTS_FOLDER_NAME.value() + File.separator;
		} else {
			gfsEnv = AppConfig.ROOT_FOLDER.value() + File.separator + AppConfig.GFS_FOLDER.value() + File.separator
					+ companyId + File.separator + AppConfig.REPORTS_FOLDER_NAME.value() + File.separator;
		}

		if (frequency.equalsIgnoreCase(AppConfig.DAILY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_DAILY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.WEEKLY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_WEEKLY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.MONTHLY_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_MONTHLY_REPORTS_FOLDER.value();
		} else if (frequency.equalsIgnoreCase(AppConfig.ANNUAL_REPORTS_FREQUENCY_KEY.value())) {
			gfsEnv += AppConfig.GFS_ANNUAL_REPORTS_FOLDER.value();
		}

		Gfs gfs = null;

		if (isGfsLocal) {
			gfs = Gfs.newLocalStorageBuilder(gfsEnv).build();
		} else {
			if (isDev) {
				gfs = Gfs.newS3StorageBuilder(AppConfig.BUCKET_NAME.value(), gfsEnv)
						.withCredentials(AwsCredentialsConfig.ACCESS_KEY_ID.value(),
								AwsCredentialsConfig.SECRET_KEY_ID.value())
						.withProxy(AwsCredentialsConfig.IP.value(), AwsCredentialsConfig.PORT.value()).build();
			} else {
				gfs = Gfs.newS3StorageBuilder(AppConfig.BUCKET_NAME.value(), gfsEnv).build();
			}
		}

		List<String> gfsKeys = gfs.listKeys();
		Set<String> companyReportsGfsKeys = new HashSet<String>();
		for (String gfsKey : gfsKeys) {
			String[] splittedKey = gfsKey.split("/");
			gfsKey = splittedKey[splittedKey.length - 1];
			if (gfsKey.startsWith(companyId) && gfsKey.contains(reportType)) {
				companyReportsGfsKeys.add(gfsKey);
			}
		}
		
		Map<String, Map<String, String>> gfsKeyAndFilesListMap = new HashMap<String, Map<String, String>>();

		for (String companyReportsGfsKey : companyReportsGfsKeys) {
			for (String month : Arrays.asList(months)) {
				if (companyReportsGfsKey.contains(month)) {
					needsSortingByMonthName = true;
					break;
				}
			}
			
			Map<String, String> fileIdAndFileNameMap = new HashMap<String, String>();
			List<FileObject> fileObjs = gfs.retrieve(companyReportsGfsKey);

			for (FileObject fileObj : fileObjs) {
				fileIdAndFileNameMap.put(fileObj.getFileId(), fileObj.getFileName());
			}

			gfsKeyAndFilesListMap.put(companyReportsGfsKey, fileIdAndFileNameMap);
		}

		gfs.close();
		
		String reportsList = null;
		Map<String, Map<String, String>> sorted = null;
		
		if (needsSortingByMonthName) {
			sorted = sortKeysByMonthString(gfsKeyAndFilesListMap, companyId, reportType);
		} else {
			sorted = sortKeysByDate(gfsKeyAndFilesListMap, companyId, reportType);
		}
		
		ObjectMapper mapper = new ObjectMapper();
//		String reportsList = mapper.writeValueAsString(gfsKeyAndFilesListMap);
		reportsList = mapper.writeValueAsString(sorted);
		
		return Response.ok(reportsList).build();
	}
	
	public Map<String, Map<String, String>> sortKeysByMonthString(Map<String, Map<String, String>> gfsKeyAndFilesListMap,
			String companyId, String reportType) {
		Map<String, Map<String, String>> sortedKeysByStringMonth = new LinkedHashMap<>();
		Map<String, List<String>> sortedKeysByYear = new TreeMap<>(Collections.reverseOrder());
		
		for (String gfsKey : gfsKeyAndFilesListMap.keySet()) {
			String year = gfsKey.replace(companyId, "").replace(reportType, "").substring(0, 4);
			if (sortedKeysByYear.containsKey(year)) {
				sortedKeysByYear.get(year).add(gfsKey);
			} else {
				List<String> keys = new ArrayList<>();
 				keys.add(gfsKey);
 				sortedKeysByYear.put(year, keys);
			}
		}
		
		for (String year : sortedKeysByYear.keySet()) {
			List<String> sortedKeysByMonth = sortKeysByMonth(sortedKeysByYear.get(year));
			for (String key : sortedKeysByMonth) {
				sortedKeysByStringMonth.put(key, gfsKeyAndFilesListMap.get(key));
			}
		}
		
		return sortedKeysByStringMonth;
		
	}
	
	private List<String> sortKeysByMonth(List<String> keys) {
		List<String> sortedKeys = new LinkedList<>();
		
		for (int i=months.length; i>0; i--) {
			List<String> keysByMonth = new ArrayList<>();
			for (String key : keys) {
				if (key.contains(months[i-1])) {
					keysByMonth.add(key);
				}
			}
			Collections.sort(keysByMonth, Collections.reverseOrder());
			sortedKeys.addAll(keysByMonth);
		}
		
		return sortedKeys;
	}

	private Map<String, Map<String, String>> sortKeysByDate(Map<String, Map<String, String>> gfsKeyAndFilesListMap,
			String companyId, String reportType) {
		Map<String, Map<String, String>> sortedKeysByNumericDate = new LinkedHashMap<>();
		Map<String, List<String>> sortedKeysByYear = new TreeMap<>(Collections.reverseOrder());
		
		for (String gfsKey : gfsKeyAndFilesListMap.keySet()) {
			String date = gfsKey.replace(companyId, "").replace(reportType, "");
			String year = date.substring(date.indexOf("-", date.indexOf("-") + 1 ) + 1, date.indexOf("_"));
			if (sortedKeysByYear.containsKey(year)) {
				sortedKeysByYear.get(year).add(gfsKey);
 			} else {
 				List<String> keys = new ArrayList<>();
 				keys.add(gfsKey);
 				sortedKeysByYear.put(year, keys);
 			}
		}
		
		for (String year : sortedKeysByYear.keySet()) {
			Collections.sort(sortedKeysByYear.get(year), Collections.reverseOrder());
			for (String gfsKey : sortedKeysByYear.get(year)) {
				sortedKeysByNumericDate.put(gfsKey, gfsKeyAndFilesListMap.get(gfsKey));
			}
		}
		
		return sortedKeysByNumericDate;
		
	}

	@POST
	@Path("/report/runnable/{reportType}")
	public Response executeRunnableJar(String parametersList, @PathParam("reportType") String reportType)
			throws IOException, InterruptedException {
		String jarFilePath = RunnableJarsConfig.getRunnableJarPath(reportType);

		if (StringUtils.isBlank(jarFilePath)) {
			return Response.status(410).build();
		} else {
			File jarFile = new File(jarFilePath);

			ObjectMapper mapper = new ObjectMapper();
			TypeFactory typeFactory = mapper.getTypeFactory();
			List<String> jarParametersList = mapper.readValue(parametersList,
					typeFactory.constructCollectionType(List.class, String.class));

			List<String> parameters = new ArrayList<String>();
			parameters.add("java");
			parameters.add("-jar");
			parameters.add(jarFile.getName());

			for (String param : jarParametersList) {
				parameters.add(param);
			}

			ProcessBuilder pb = new ProcessBuilder(parameters);
			pb.directory(jarFile.getParentFile());
			// Process process = pb.start();
			// process.waitFor();

			Process p = pb.start();
			// LogStreamReader lsr = new LogStreamReader(p.getInputStream());

			InputStream is;
			byte[] bytes = IOUtils.toByteArray(p.getInputStream());

			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HHmmss");
			Date date = new Date();
			String logFileName = "Log-" + reportType + " " + sdf.format(date) + ".txt";

			String logFilePath = AppConfig.RUNNABLE_JARS_LOGS_PATH.value() + File.separator + logFileName;

			try (FileOutputStream fileOuputStream = new FileOutputStream(logFilePath)) {
				fileOuputStream.write(bytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return Response.ok(logFileName).build();
		}
		
	}

	@GET
	@Path("/report/runnable/logs/{logFileName}")
	public Response executeRunnableJar(@PathParam("logFileName") String logFileName)
			throws IOException, InterruptedException {
		String mt = "application/octet-stream";
		ContentDisposition contentDisposition = null;

		File logFile = new File(AppConfig.RUNNABLE_JARS_LOGS_PATH.value() + File.separator + logFileName);

		if (logFile.exists()) {
			InputStream in = new FileInputStream(logFile);
			contentDisposition = ContentDisposition.type("inline").fileName(logFileName).creationDate(new Date())
					.build();
			return Response.ok(in, mt).header("Content-Disposition", contentDisposition).build();
		}
		
		return Response.status(Status.NOT_FOUND).build();

	}
	
	@GET
	@Path("/viewphoto")
	public Response viewPhoto(@QueryParam("e") String employeeId, @QueryParam("t") long timestamp,
			@QueryParam("a") int actionType, @QueryParam("c") String companyId) {

		Date date = new Date(timestamp);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String dateString = String.format("%02d", calendar.get(Calendar.MONTH) + 1)
				+ String.valueOf(calendar.get(Calendar.YEAR));
		String root = System.getProperty("rootPath");

		String xmlFilePath = AppConfig.TNA_XML_DIR.value() + File.separator + companyId + File.separator
				+ AppConfig.TNA_PARSER_FOLDER.value() + File.separator + employeeId + File.separator + dateString + "_"
				+ companyId + "_" + employeeId + "_TNA.xml";

		if ((actionType != 0) && (actionType != 1)) {
			return buildImgResponse(root + AppConfig.ERROR_IMG.value());
		}
		
		try {
			XmlParser xmlParser = new XmlParser(xmlFilePath);
			String empImageFileName = xmlParser.getImageFileName(timestamp, actionType);
			
			if (empImageFileName.equals("")) {
				return buildImgResponse(root + AppConfig.NOT_FOUND_IMG.value());
			} else {
				String empImageFilePath = AppConfig.TK_PHOTO_DIR.value() + File.separator + employeeId + File.separator
						+ empImageFileName;
				return buildImgResponse(empImageFilePath);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return buildImgResponse(root + AppConfig.ERROR_IMG.value());
		}
	}

	private Response buildImgResponse(String imageFile) {
		String mt = "";
		InputStream in = null;
		ContentDisposition contentDisposition = null;

		try {
			in = new FileInputStream(imageFile);
			mt = URLConnection.guessContentTypeFromName(imageFile);
			contentDisposition = ContentDisposition.type("inline").fileName(imageFile).creationDate(new Date()).build();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return Response.ok(in, mt).header("Content-Disposition", contentDisposition).build();
	}

}
