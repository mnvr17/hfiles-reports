package com.svi.hfiles.utils;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.mapping.MappingManager;
import com.svi.hfiles.cassandra.objects.HfilesDatabaseAccessor;
import com.svi.hfiles.globals.AppConfig;

public class CassandraConnectionUtil {
  private static SocketOptions socketOptions = (new SocketOptions()).setReadTimeoutMillis(30000);
  
  private static Cluster cluster = Cluster.builder().addContactPoint((String)AppConfig.CASSANDRA_IP_ADD.value())
    .withSocketOptions(socketOptions)
    .build();
  
  private static Session session = cluster.connect((String)AppConfig.CASSANDRA_KEYSPACE.value());
  
  private static MappingManager manager = new MappingManager(session);
  
  private static HfilesDatabaseAccessor userAccessor = (HfilesDatabaseAccessor)manager.createAccessor(HfilesDatabaseAccessor.class);
  
  public static void close() {
    session.close();
    cluster.close();
  }
  
  public static ResultSet getTaxComputationDetail(String query) {
    return session.execute(query);
  }
  
  public static MappingManager getManager() {
    return manager;
  }
  
  public static HfilesDatabaseAccessor getUserAccessor() {
    return userAccessor;
  }
  
  public static Session getSession(){
	  return session;
  }
}
