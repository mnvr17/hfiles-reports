package com.svi.hfiles.utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlParser {
	
	private Document document;
	
	public XmlParser(String xmlFilePath) throws ParserConfigurationException, SAXException, IOException {
		File xmlFile = new File(xmlFilePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = dbFactory.newDocumentBuilder();
		document = builder.parse(xmlFile);
	}
	
	public String getImageFileName(long timestamp, int actionType) {
		String imageFileName = "";
		String action = (actionType == 0) ? "Time_In" : "Time_Out";
		
		NodeList nodeList = document.getElementsByTagName("Work_Hrs");
		for (int i = 0; i<nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				if (getText(element, action).equals(String.valueOf(timestamp))) {
					return getText(element, "Photo_" + action);
				}
			}
		}

		return imageFileName;
	}

	private String getText(Element element, String tagName) {
		if (element.getElementsByTagName(tagName).item(0) != null) {
			return element.getElementsByTagName(tagName).item(0).getTextContent();
		} else {
			return "";
		}
	}

}
